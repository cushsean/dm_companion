from datetime import datetime, timedelta

from app.blueprints.auth.models import PagePermissions, PrivilegeLevel, User

def test_user_model():
    """
    GIVEN a User model
    WHEN a new user is created
    THEN check the username, email, hashed_password, date_joined, privilege_id, 
         and disabled values are correct
    """

    date_stamp = datetime.now()

    privilege_tmp = PrivilegeLevel()

    user = User(
        username = 'username',
        email = 'test_email@mail.com',
        password = 'password',
        date_joined = date_stamp + timedelta(days = 30),
        privilege = privilege_tmp,
        disabled = False
    )
    
    assert user.username == 'username'
    assert user.email == 'test_email@mail.com'
    assert user.password != 'password'
    assert user.date_joined > date_stamp - timedelta(seconds=60)
    assert user.date_joined < date_stamp + timedelta(seconds=60)
    # Should return None since no privilegeLevels exist.
    assert user.privilege_id == None
    assert user.disabled == False


def test_privilege_level_model():
    """
    GIVEN a PrivilegeLevel model
    WHEN a new privilege is created
    THEN check the name and level values are correct
    """

    privilege_level = PrivilegeLevel(
        name = "test_name",
        level = 12
    )

    assert privilege_level.name == "test_name"
    assert privilege_level.level == 12


def test_page_permissions_model():
    """
    GIVEN a PagePermissions model
    WHEN a new page permission is created
    THEN check the page, read_level, and write_level values are correct
    """

    page_permissions = PagePermissions(
        page = "test_url",
        read_level = 1,
        write_level = 1
    )

    assert page_permissions.page == "test_url"
    assert page_permissions.read_level == 1
    assert page_permissions.write_level == 1
