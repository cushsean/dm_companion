from flask import request, url_for

from app.blueprints.auth.models import User

class TestSignIn:
    def test_get(self, client):
        """
        GIVEN a GET request for 'auth/sign_in'
        WHEN the request is satified
        THEN responde with 200 and no errors.
        """

        # Setup
        status_code = 200
        url_get = url_for('auth.sign_in')
        url_resp = url_get

        # Act
        resp = client.get(url_get, follow_redirects=True)

        # Assert
        assert b'error' not in resp.data
        assert resp.request.path == url_resp
        assert resp.status_code == status_code

    def test_valid_creds(self, client, user_player):
        """
        GIVEN a POST request for 'auth/sign_in'
        WHEN the user credentials are valid
        THEN responde with 200 and redirect to '/home'.
        """

        # Setup
        status_code = 200
        url_post = url_for('auth.sign_in')
        url_resp = url_for('home.index')

        data = dict(
            username = user_player['username'],
            password = user_player['password']
        )

        # Act
        resp = client.post(url_post, data=data, follow_redirects=True)

        # Assert
        assert b'error' not in resp.data
        assert resp.request.path == url_resp
        assert resp.status_code == status_code

    def test_invaild_username(self, client, user_player):
        """
        GIVEN a POST request for 'auth/sign_in'
        WHEN the user's username is invalid
        THEN responde with 200 and display error.
        """

        # Setup
        status_code = 200
        url_post = url_for('auth.sign_in')
        url_resp = url_post

        data = dict(
            username = user_player['username'] + 'x',
            password = user_player['password']
        )

        # Act
        resp = client.post(url_post, data=data, follow_redirects=True)

        # Assert
        assert b'error' in resp.data
        assert resp.request.path == url_resp
        assert resp.status_code == status_code

    def test_invaild_password(self, client, user_player):
        """
        GIVEN a POST request for 'auth/sign_in'
        WHEN the user's password is invalid
        THEN responde with 200 and display error.
        """

        # Setup
        status_code = 200
        url_post = url_for('auth.sign_in')
        url_resp = url_post

        data = dict(
            username = user_player['username'],
            password = user_player['password'] + 'x'
        )

        # Act
        resp = client.post(url_post, data=data, follow_redirects=True)

        # Assert
        assert b'error' in resp.data
        assert resp.request.path == url_resp
        assert resp.status_code == status_code
