from flask import request, url_for

from app.blueprints.auth.models import User

class TestJoin:
    def test_get(self, client):
        """
        GIVEN a GET request for '/auth/join'
        WHEN the request is satisfied
        THEN responde with 200 and no errors. 
        """

        # Setup
        status_code = 200
        url_get = url_for('auth.join')
        url_resp = url_get

        # Act
        resp = client.get(url_get, follow_redirects=True)

        # Assert
        assert resp.status_code == status_code
        assert b'error' not in resp.data


    def test_new_user(self, client, session, privilege_all):
        """
        GIVEN a POST request for '/auth/join'
        WHEN a new unique user is created
        THEN responde with 200, redirect to home, log the new user in, and 
             store in db. 
        """

        # Setup
        status_code = 200
        url_post = url_for('auth.join')
        url_resp = url_for('home.index')
        username = 'new_user'
        password = 'password'
        email = 'test@mail.com'

        data = dict(
            username = username,
            password = password,
            email = email,
            confirm_password = password,
            confirm_email = email
        )

        # Act
        resp = client.post(url_post, data=data, follow_redirects=True)

        # Assert
        assert resp.status_code == status_code
        assert resp.request.path == url_resp
        assert username.encode() in resp.data
        assert username == User.query.filter_by(username=username).first().username
        assert b'error' not in resp.data


    def test_nonunique_username(self, client, session, privilege_all):
        """
        GIVEN a POST request for '/auth/join'
        WHEN a new non-unique user is created
        THEN responde with 200, show error message, and do not duplicate user in db. 
        """

        # Setup
        status_code = 200
        url_post = url_for('auth.join')
        url_resp = url_post
        username = 'new_user'
        password = 'password'
        email = 'test@mail.com'

        data = dict(
            username = username,
            password = password,
            email = email,
            confirm_password = password,
            confirm_email = email
        )
        # Ensuring a duplicate user
        client.post(url_post, data=data, follow_redirects=True)

        # Act
        resp = client.post(url_post, data=data, follow_redirects=True)

        # Assert
        assert resp.status_code == status_code
        assert resp.request.path == url_resp
        assert b'error' in resp.data
        assert url_for('auth.sign_in').encode() in resp.data

        # 2 != rather than 1 == due to db.session.rollback() wiping both users
        # from the database rather than just the duplicate. This is producing
        # the desired result, two records would represent an issue.
        assert 2 != User.query.filter_by(username=username).count()
        