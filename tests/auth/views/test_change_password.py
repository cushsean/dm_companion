import pytest

from flask import request, url_for
from flask_login import current_user

from app.blueprints.auth.models import User
from app.blueprints.auth.forms.update_password import UpdatePasswordForm

class TestChangePassword:
    def test_not_logged_in(self, client):
        """
        GIVEN a GET request for '/auth/change_password'
        WHEN there is no user logged in
        THEN respond with 200 (success) and redirct to '/auth/sign_in' 
             with no errors.
        """

        # Setup
        status_code = 200
        url_get = url_for('auth.change_password')
        url_resp = url_for('auth.sign_in')

        # Act
        resp = client.get(url_get, follow_redirects=True)

        # Assert
        assert resp.status_code == status_code
        assert b'error' not in resp.data
        assert resp.request.path == url_resp
        
    def test_inital_get_request(self, client, disable_login):
        """
        GIVEN an inital GET request for '/auth/change_password'
        WHEN a user is logged in
        THEN respond with 200 (success) with no errors.
        """

        # Setup
        status_code = 200
        url = url_for('auth.change_password')

        # Act
        resp = client.get(url, follow_redirects=True)
        
        # Assert
        assert resp.status_code == status_code
        assert b'error' not in resp.data
        assert resp.request.path == url

    def test_new_password(self, client, session, utils, user_player):
        """
        GIVEN a POST request to '/auth/change_password'
        WHEN valid cridential are provided
        THEN redirect to '/user/profile' and change the users password 
             in the db.
        """

        # Setup
        status_code = 200
        url_post = url_for('auth.change_password')
        url_resp = url_for('user.profile')
        
        utils.login(client, user_player)
        user = utils.get_user(user_player)

        current_password = user_player['password']
        new_password = user_player['password'] + 'x'

        data = dict(
            current_password = current_password,
            new_password = new_password,
            confirm_password = new_password
        )

        # Act
        resp = client.post(url_post, data=data, follow_redirects=True)

        # Assert
        assert resp.status_code == status_code
        assert resp.request.path == url_resp
        assert user.validate_password(new_password) == True
        assert b'error' not in resp.data

    def test_invalid_password(self, client, session, utils, user_player):
        """
        GIVEN a POST request to '/auth/change_password'
        WHEN the current password is not correct
        THEN respond with error message and status code 200.
        """

        # Setup
        status_code = 200
        url_post = url_for('auth.change_password')
        url_resp = url_post
        
        utils.login(client, user_player)

        current_password = user_player['password'] + 'x'
        new_password = user_player['password'] + 'z'

        data = dict(
            current_password = current_password,
            new_password = new_password,
            confirm_password = new_password
        )

        # Act
        resp = client.post(url_post, data=data, follow_redirects=True)

        # Assert
        assert resp.status_code == status_code
        assert resp.request.path == url_resp
        assert b'error' in resp.data
