import pytest

from flask import request, url_for

from app.blueprints.auth.models import User

class TestSignIn:
    def test_get(self, client, utils, user_player):
        """
        GIVEN a get request for 'auth/sign_out'
        WHEN a user is signed in
        THEN sign the user out and redirect to '/home' with 200
        """

        # Setup
        status_code = 200
        url_get = url_for('auth.sign_out')
        url_resp = url_for('home.index')

        utils.login(client, user_player)

        # Act
        resp = client.get(url_get, follow_redirects=True)

        # Assert
        assert b'error' not in resp.data
        assert resp.request.path == url_resp
        assert resp.status_code == status_code
        # Checks that the user was logged out.
        assert user_player['username'].encode() not in resp.data

    def test_not_logged_in(self, client):
        """
        GIVEN a get request for 'auth/sign_out'
        WHEN a user is not signed in
        THEN redirect to '/auth/sign_in' with 200.
        """

        # Setup
        status_code = 200
        url_get = url_for('auth.sign_out')
        url_resp = url_for('auth.sign_in')

        # Act
        resp = client.get(url_get, follow_redirects=True)

        # Assert
        # TODO: The following assert only fails when tests outside this module 
        # are executed. This indicates that data is being leaked between tests.
        # The 'errors' are not displayed while following the actions of this 
        # test from the non-test environment. This test has been marked as xfile
        # until this issue is isolated and resolved.
        assert b'error' not in resp.data
        assert resp.request.path == url_resp
        assert resp.status_code == status_code
