import pytest

from flask_login import current_user, logout_user

from app import db
from app.blueprints.auth import check_permissions, is_dm, is_admin, login_attempt, load_user
from app.blueprints.auth.models import PagePermissions, PrivilegeLevel, User


# Local Fixtures
@pytest.fixture(scope='function')
def test_page(session):
    test_page = PagePermissions(
        page = "test",
        read_level = 5,
        write_level = 1
    )

    db.session.add(test_page)
    db.session.commit()

    yield test_page

    db.session.delete(test_page)
    db.session.commit()

# Tests
class TestLoginAttempt:
    def test_player_user(self, user_player):
        """
        GIVEN an existing user
        WHEN the user attemps to login
        THEN verify cridentials
        """

        user = user_player

        # Attempt login
        error = login_attempt(user['username'], user['password'])

        # Assert
        assert error == None

        # Teardown
        if current_user.is_authenticated:
            logout_user()



    def test_invalid_username(self, user_player):
        """
        GIVEN an existing user
        WHEN the user attemps to login with a non-existant username
        THEN verify "Username or Password Invalid." is returned.
        """

        user = user_player

        # Attempt login
        error = login_attempt(user['username'] + 'x', user['password'])

        # Assert
        assert error == "Username or Password Invalid."

        # Teardown
        if current_user.is_authenticated:
            logout_user()



    def test_invalid_password(self, user_player):
        """
        GIVEN an existing user
        WHEN the user attemps to login with an incorrect password
        THEN verify "Username or Password Invalid." is returned.
        """

        user = user_player

        # Attempt login
        error = login_attempt(user['username'], user['password'] + 'x')

        # Assert
        assert error == "Username or Password Invalid."

        # Teardown
        if current_user.is_authenticated:
            logout_user()



    def test_invalid_username_and_password(self, user_player):
        """
        GIVEN an existing user
        WHEN the user attemps to login with both an invalid username and password
        THEN verify "Username or Password Invalid." is returned.
        """

        user = user_player

        # Attempt login
        error = login_attempt(user_player['username'] + 'x', user_player['password'] + 'x')

        # Assert
        assert error == "Username or Password Invalid."

        # Teardown
        if current_user.is_authenticated:
            logout_user()



    def test_disabled_user(self, user_player):
        """
        GIVEN an existing user
        WHEN the user is disabled and attempts to login
        THEN return "This account is disabled. Please contact an administrator."
        """

        # Setup
        user = user_player        
        _user = User.query.filter_by(username=user['username']).first()
        _user.disabled = True    
        db.session.commit()

        # Act
        error = login_attempt(user['username'], user['password'])

        # Assert
        assert error == "This account is disabled. Please contact an administrator."

        # Teardown
        _user.disabled = False
        db.session.commit()
        if current_user.is_authenticated:
            logout_user()


@pytest.mark.parametrize(
    "user_level, validation_func, expected", [
        ('user_player', is_admin, False), 
        ('user_dm', is_admin, False), 
        ('user_admin', is_admin, True),
        ('user_player', is_dm, False), 
        ('user_dm', is_dm, True), 
        ('user_admin', is_dm, True)]
)
def test_user_privilege_validation(user_level, validation_func, expected, request, privilege_all):
    """
    GIVEN an existing user with a given privilege level
    WHEN the user attemps to authenticate as an admin
    THEN return True if user is an admin and False otherwise.
    """

    user = request.getfixturevalue(user_level)

    try:
        login_attempt(user['username'], user['password'])
    except:
        assert False
    
    assert validation_func() == expected
    
    # Teardown
    if current_user.is_authenticated:
        logout_user()

class TestLoadUser:
    def test_user_exisits(self, user_player):

        # Setup
        user = user_player
        _user = User.query.filter_by(username=user['username']).first()
        userid = User.query.filter_by(privilege_id=user['privilege'].id).first().id
        
        # Asserts
        assert load_user(userid) == _user

        # Teardown
        if current_user.is_authenticated:
            logout_user()


    def test_user_doesnt_exisits(self, session):
        assert load_user(0) == None

        # Teardown
        if current_user.is_authenticated:
            logout_user()


class TestCheckPermissions:
    def test_page_none(self):
        """
        GIVEN An authenticated user and a page with specified permissions
        WHEN no page is given
        THEN return None.
        """

        assert check_permissions(None) == None


    def test_page_nonexistent(self, user_player):
        """
        GIVEN An authenticated user and a page with specified permissions
        WHEN the given page does not exist
        THEN return None.
        """

        # Setup
        user = user_player
        login_attempt(user['username'], user['password'])

        # Act & Assert
        assert check_permissions("nonexistant_page") == None

        # Teardown
        if current_user.is_authenticated:
            logout_user()


    def test_user_not_authenticated(self, test_page):
        """
        GIVEN An authenticated user and a page with specified permissions
        WHEN the user attempts to access the page
        THEN determine what permissions the user has on the page.
        """

        assert check_permissions(test_page.page) == (False, False)


    @pytest.mark.parametrize(
        "user_level, read_access, write_access",
        [
            ('user_player', False, False),
            ('user_dm', True, False),
            ('user_admin', True, True)
        ]
    )
    def test_valid_users(self, 
                         user_level, 
                         read_access, 
                         write_access, 
                         request, 
                         test_page):
        # Setup
        user = request.getfixturevalue(user_level)
        login_attempt(user['username'], user['password'])

        # Act & Assert
        assert check_permissions(test_page.page) == (read_access, write_access)

        # Teardown
        if current_user.is_authenticated:
            logout_user()