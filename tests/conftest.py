import os
import pytest

from flask import current_app
from flask.testing import FlaskClient
from flask_login import current_user, logout_user

from app import create_app, db
from app.blueprints.auth import login_attempt
from app.blueprints.auth.models import PagePermissions, PrivilegeLevel, User


@pytest.fixture(scope='module')
def test_app():
    app = create_app(test_config='../tests/config.py')
    with app.app_context():
        yield app

@pytest.fixture(scope='module', autouse=True)
def client(test_app):
    """Instatiating and configuring the application."""
    
    app = test_app
    ctx = test_app.test_request_context()
    ctx.push()
    app.test_client_class = FlaskClient
    return app.test_client()

@pytest.fixture(scope='function')
def disable_login():
    # TODO: Inverse this logic so that login is disabled by default and 
    #       a fixture enable_login can enable it.
    current_app.config['LOGIN_DISABLED'] = True
    yield
    current_app.config['LOGIN_DISABLED'] = False


## Helpers
class Utils:
    """For all the helper functions for testing"""

    @staticmethod
    def login(client, user_attributes):
        user = user_attributes
        data = dict(
            username=user['username'],
            password=user['password']
        )
        client.post('/auth/sign_in', data=data, follow_redirects=True)
        return 

    @staticmethod
    def logout():
        if current_user.is_authenticated:
            login_user()
        return

    @staticmethod
    def get_user(user_attributes):
        user = User.query.filter_by(username=user_attributes['username']).first()
        return user



@pytest.fixture(scope='function')
def utils():
    return Utils

## Database
# Initialization
@pytest.fixture(scope='module')
def init_db(client):
    db.create_all()
    yield
    db.drop_all()

@pytest.fixture(scope='function')
def session(init_db):
    connection = db.engine.connect()
    transaction = connection.begin()
    options = dict(bind=connection, binds={})
    session = db.create_scoped_session(options=options)
    db.session = session

    yield session

    # Teardown
    transaction.rollback()
    connection.close()
    session.remove()

@pytest.fixture(scope='function')
def privilege_player(session):
    """Add the pivilege level player at value 10"""

    privilege_level = PrivilegeLevel(
        name = "Player",
        level = 10
    )
    db.session.add(privilege_level)
    db.session.commit()
    return


# Privilege
@pytest.fixture(scope='function')
def privilege_dm(session):
    """Add the pivilege level dm at value 5"""

    privilege_level = PrivilegeLevel(
        name = "DM",
        level = 5
    )
    db.session.add(privilege_level)
    db.session.commit()
    return


@pytest.fixture(scope='function')
def privilege_admin(session):
    """Add the pivilege level admin at value 1"""

    privilege_level = PrivilegeLevel(
        name = "Admin",
        level = 1
    )
    db.session.add(privilege_level)
    db.session.commit()
    return


@pytest.fixture(scope='function')
def privilege_all(session, 
                  privilege_player, 
                  privilege_admin, 
                  privilege_dm):
    """Add all pivilege levels"""
    # Intentially left blank
    return


# Users
def get_user_model(user_attributes: dict):
    user = User(
        username = user_attributes['username'],
        password = user_attributes['password'],
        email = user_attributes['email'],
        privilege = user_attributes['privilege']
    )
    return user

@pytest.fixture(scope='function')
def import_users(user_player, user_dm, user_admin):
    """Addes all users to the database"""
    return (user_player, user_dm, user_admin)

@pytest.fixture(scope='function')
def user_player(session, privilege_player):
    """Add a Player to the database to be used to test against."""

    # Create the user model
    user_attributes = {
        "username": "test_user_player",
        "password": "passtest",
        "email": "test_user_player@email.com",
        "privilege": PrivilegeLevel.query.filter_by(name="Player").first()
    }

    user = get_user_model(user_attributes)

    db.session.add(user)
    db.session.commit()
    
    return user_attributes

@pytest.fixture(scope='function')
def user_dm(session, privilege_dm):
    """Add a DM to the database to be used to test against."""

    # Create the user model
    user_attributes = {
        "username": "test_user_dm",
        "password": "passtest",
        "email": "test_user_dm@email.com",
        "privilege": PrivilegeLevel.query.filter_by(name="DM").first()
    }

    user = get_user_model(user_attributes)

    db.session.add(user)
    db.session.commit()
    
    return user_attributes

@pytest.fixture(scope='function')
def user_admin(session, privilege_admin):
    """Add a Admin to the database to be used to test against."""

    # Create the user model
    user_attributes = {
        "username": "test_user_admin",
        "password": "passtest",
        "email": "test_user_admin@email.com",
        "privilege": PrivilegeLevel.query.filter_by(name="Admin").first()
    }

    user = get_user_model(user_attributes)

    db.session.add(user)
    db.session.commit()
    
    return user_attributes
