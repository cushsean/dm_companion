import pytest

from flask import request, url_for

from app import db
from app.blueprints.economy.models import Currency

class TestCurrency:
    @pytest.mark.parametrize(
        'user, status_code',
        [
            (None, 404),
            ('user_player', 404),
            ('user_dm', 200),
            ('user_admin', 200)
        ]
    )
    def test_get_access(self, client, utils, request, privilege_dm, user, status_code):
        """
        GIVEN a request for '/economy/currency'
        WHEN the request is POST
        THEN resonde with 404 if the user's privilege is below 'DM', otherwise
             resonde with 200; in both cases, no errors.
        """

        # Setup 
        url = url_for('economy.currency')
        
        if user:
            utils.login(client, request.getfixturevalue(user))

        # Act
        resp = client.get(url, follow_redirects=True)

        # Assert 
        assert resp.status_code == status_code
        assert resp.request.path == url
        assert b'error' not in resp.data

    def test_get_data(self, client, disable_login, session):
        """
        GIVEN a view for 'economy/currency'
        WHEN a GET request is made, and the user is authenticated,
        THEN display all the current Currency data from the database.
        """

        # Setup
        url = url_for('economy.currency')
        status_code = 200

        data_headers = ['Item', 'Description', 'Gold Standard']
        data = dict(
            item = 'new_item', 
            description = 'This is a new item',
            gold_std = 3.14
        )

        model = Currency(
            item = data['item'],
            description = data['description'],
            gold_std = data['gold_std']
        )

        db.session.add(model)
        
        # Act
        resp = client.get(url)

        # Assert
        assert resp.status_code == status_code
        assert resp.request.path == url
        for header in data_headers:
            assert header.encode() in resp.data
        for key, _data in data.items():
            assert str(_data).encode() in resp.data
        
    def test_post_data(self, client, disable_login, session):
        """
        GIVEN a view for 'economy/currency'
        WHEN a POST request is made, and the user is authenticated,
        THEN display all the current Currency data from the database, including 
             the one that is added.
        """

        # Setup
        url = url_for('economy.currency')
        status_code = 200

        data_headers = ['Item', 'Description', 'Gold Standard']
        data = dict(
            item = 'old_item', 
            description = 'This is the old item',
            gold_std = 3.14
        )

        model = Currency(
            item = data['item'],
            description = data['description'],
            gold_std = data['gold_std']
        )

        db.session.add(model)

        new_data = dict(
            item = 'new_item', 
            desc = 'This is the new item',
            gold_std = 6.28
        )
        
        # Act
        resp = client.post(url, data=new_data, follow_redirects=True)
        
        # Assert
        assert resp.status_code == status_code
        assert resp.request.path == url
        for header in data_headers:
            assert header.encode() in resp.data
        for key, _data in data.items():
            assert str(_data).encode() in resp.data
        for key, _data in new_data.items():
            assert str(_data).encode() in resp.data

    def test_post_nonunique_data(self, client, disable_login, session):
        """
        GIVEN a view for 'economy/currency'
        WHEN a POST request is made with non-unique data, 
             and the user is authenticated,
        THEN display all the current Currency data from the database, an error
             message and do not add the new data to the database.
        """

        # Setup
        url = url_for('economy.currency')
        status_code = 200

        data_headers = ['Item', 'Description', 'Gold Standard']
        data = dict(
            item = 'old_item', 
            description = 'This is the old item',
            gold_std = 3.14
        )

        model = Currency(
            item = data['item'],
            description = data['description'],
            gold_std = data['gold_std']
        )

        db.session.add(model)

        new_data = dict(
            item = 'old_item', 
            desc = 'This is the old item',
            gold_std = 3.14
        )
        
        # Act
        resp = client.post(url, data=new_data, follow_redirects=True)
        
        # Assert
        assert resp.status_code == status_code
        assert resp.request.path == url
        for header in data_headers:
            assert header.encode() in resp.data
        for key, _data in data.items():
            assert str(_data).encode() in resp.data
        assert b'error' in resp.data