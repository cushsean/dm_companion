import pytest

from flask import request, url_for

class TestEconomy:
    @pytest.mark.parametrize(
        'user, status_code',
        [
            (None, 404),
            ('user_player', 404),
            ('user_dm', 200),
            ('user_admin', 200)
        ]
    )
    def test_get_user_validation(self, client, utils, request, privilege_dm, user, status_code):
        """
        GIVEN a get request for '/economy'
        WHEN a user accesses the view
        THEN resonde with 404 if the user's privilege is below 'DM', otherwise
             resonde with 200; in both cases, no errors.
        """

        # Setup
        if user:
            utils.login(client, request.getfixturevalue(user))

        url = url_for('economy.index')
        
        # Act
        resp = client.get(url, follow_redirects=True)

        # Assert
        assert resp.status_code == status_code
        assert resp.request.path == url
        assert b'error' not in resp.data

    def test_post(self, client, disable_login):
        """
        GIVEN a request for '/economy'
        WHEN the request is POST
        THEN respond with 405 and 'Method Not Allowed'.
        """

        # Setup
        url = url_for('economy.index')
        status_code = 405
        error = b'Method Not Allowed'

        # Act
        resp = client.post(url, follow_redirects=True)

        # Assert
        assert resp.status_code == status_code
        assert error in resp.data