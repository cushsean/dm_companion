from app.blueprints.economy.models import Currency

def test_currency_model():
    """
    GIVEN a Currency model
    WHEN a new currency is created
    THEN check that the values are correct
    """

    currency = Currency(
        item = "new_coin",
        description = "A coin that is new.",
        gold_std = 0.5
    )

    assert currency.item == "new_coin"
    assert currency.description == "A coin that is new."
    assert currency.gold_std == 0.5
