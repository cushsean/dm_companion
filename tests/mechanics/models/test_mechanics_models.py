from app.blueprints.mechanics.models import DamageType

def test_damage_type_model():
    """
    GIVEN a DamageType  model
    WHEN a new damage type is created
    THEN check the model's values
    """

    damageType = DamageType(
        dmg_type = "new dmg type",
        dmg_description = "this is the description",
        dmg_example = "did that hurt?"
    )

    assert damageType.dmg_type == "new dmg type"
    assert damageType.dmg_description == "this is the description"
    assert damageType.dmg_example == "did that hurt?"
