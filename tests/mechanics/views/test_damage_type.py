import pytest

from flask import request, url_for

from app import db
from app.blueprints.mechanics.forms import DamageTypeForm
from app.blueprints.mechanics.models import DamageType


class TestDamageType:
    @pytest.mark.parametrize(
        "user, status_code",
        [
            (None, 404),
            ('user_player', 404),
            ('user_dm', 404),
            ('user_admin', 200)
        ]
    )
    def test_user_access(self, client, utils, request, user, status_code):
        """
        GIVEN a view for '/mechanics/damage_type'
        WHEN a request is made
        THEN validate that an 'Admin' is signed in.
        """

        # Setup
        url = url_for('mechanics.damage_type')

        if user:
            utils.login(client, request.getfixturevalue(user))

        # Act
        resp = client.get(url)

        # Assert
        assert resp.status_code == status_code
        assert resp.request.path == url
        assert b'error' not in resp.data

    def test_get(self, client, disable_login, session):
        """
        GIVEN a view for '/mechanics/damage_type'
        WHEN a get request is made
        THEN display all damaged types in the database.
        """

        # Setup
        status_code = 200
        url = url_for('mechanics.damage_type')
        url_list = [
            url_for('home.index'),
            url_for('mechanics.index')
        ]

        data = dict(
            Type = 'new_dmg_type',
            Description = 'This is the new damage_type',
            Example = 'Ka Pow!'
        )

        model = DamageType(
            dmg_type = data['Type'],
            dmg_description = data['Description'],
            dmg_example = data['Example']
        )

        db.session.add(model)

        form = DamageTypeForm()

        # Act
        resp = client.get(url)

        # Assert
        assert resp.status_code == status_code
        assert resp.request.path == url
        assert b'error' not in resp.data
        for link in url_list:
            assert link.encode() in resp.data
        for field in form:
            if field.type != 'SubmitField':
                assert field.label.text.encode() in resp.data
        for key, _data in data.items():
            assert _data.encode() in resp.data

    def test_post(self, client, disable_login, session):
        """
        GIVEN a view for '/mechanics/damage_type'
        WHEN a POST request is made
        THEN display all damaged types in the database, included the new one.
        """

        # Setup
        status_code = 200
        url = url_for('mechanics.damage_type')
        url_list = [
            url_for('home.index'),
            url_for('mechanics.index')
        ]

        data = dict(
            dmg_type= 'old_dmg_type',
            dmg_desc = 'This is the old damage_type',
            dmg_example = 'Ka Pow!'
        )

        new_data = dict(
            dmg_type = 'new_dmg_type',
            dmg_desc = 'This is the new damage_type',
            dmg_example = 'Ka BAM!'
        )

        model = DamageType(
            dmg_type = data['dmg_type'],
            dmg_description = data['dmg_desc'],
            dmg_example = data['dmg_example']
        )

        db.session.add(model)

        form = DamageTypeForm()

        # Act
        resp = client.post(url, data=new_data, follow_redirects=True)

        # Assert
        assert resp.status_code == status_code
        assert resp.request.path == url
        assert b'error' not in resp.data
        for link in url_list:
            assert link.encode() in resp.data
        for field in form:
            if field.type != 'SubmitField':
                assert field.label.text.encode() in resp.data
        for key, _data in data.items():
            assert _data.encode() in resp.data
        for key, _data in new_data.items():
            assert _data.encode() in resp.data

    def test_post_unique(self, client, disable_login, session):
        """
        GIVEN a view for '/mechanics/damage_type'
        WHEN a get request is made
        THEN only add a new damage type if it is unique.
        """

        # Setup
        status_code = 200
        url = url_for('mechanics.damage_type')
        url_list = [
            url_for('home.index'),
            url_for('mechanics.index')
        ]

        data = dict(
            dmg_type= 'old_dmg_type',
            dmg_desc = 'This is the old damage_type',
            dmg_example = 'Ka Pow!'
        )

        model = DamageType(
            dmg_type = data['dmg_type'],
            dmg_description = data['dmg_desc'],
            dmg_example = data['dmg_example']
        )

        db.session.add(model)

        form = DamageTypeForm()

        # Act
        resp = client.post(url, data=data, follow_redirects=True)

        # Assert
        assert resp.status_code == status_code
        assert resp.request.path == url
        assert b'error' in resp.data
        for link in url_list:
            assert link.encode() in resp.data
        for field in form:
            if field.type != 'SubmitField':
                assert field.label.text.encode() in resp.data
        for key, _data in data.items():
            assert _data.encode() in resp.data