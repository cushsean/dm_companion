import pytest

from flask import request, url_for


class TestMechanics:
    @pytest.mark.parametrize(
        "user, status_code",
        [
            (None, 404),
            ('user_player', 404),
            ('user_dm', 404),
            ('user_admin', 200)
        ]
    )
    def test_user_validation(self, client, request, utils, user, status_code):
        """
        GIVEN a view for '/mechanics'
        WHEN a request is made
        THEN validate the user is signed in as 'Admin', otherwise 404
        """

        # Setup
        url = url_for('mechanics.index')

        if user:
            utils.login(client, request.getfixturevalue(user))

        # Act
        resp = client.get(url)

        # Assert
        assert resp.status_code == status_code
        assert resp.request.path == url
        assert b'error' not in resp.data

    def test_get(self, client, disable_login, session):
        """
        GIVEN a view for '/mechanics'
        WHEN a get request is made
        THEN provide a link for all sub-views
        """

        # Setup
        url_get = url_for('mechanics.index')
        url_links = [
            url_for('home.index'),
            url_for('mechanics.damage_type'),
            url_for('mechanics.wnm.weight'),
            url_for('mechanics.wnm.length'),
            url_for('mechanics.wnm.time')
        ]
        status_code = 200

        # Act
        resp = client.get(url_get)

        # Assert
        assert resp.status_code == status_code
        assert b'error' not in resp.data
        assert resp.request.path == url_get
        for link in url_links:
            assert link.encode() in resp.data

    def test_post(self, client, disable_login, session):
        """
        GIVEN a view for '/mechanics'
        WHEN a post request is made
        THEN respond with 405 and 'Method Not Allowed'
        """

        # Setup
        url = url_for('mechanics.index')
        status_code = 405
        error = b'Method Not Allowed'

        # Act
        resp = client.post(url)

        # Assert
        assert resp.status_code == status_code
        assert error in resp.data
        assert resp.request.path == url
