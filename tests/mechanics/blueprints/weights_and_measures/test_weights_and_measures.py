import pytest

from flask import current_app, request, url_for

from app import db
from app.blueprints.mechanics.blueprints.weights_and_measures.models import Weight, Length, Time
from app.blueprints.mechanics.blueprints.weights_and_measures.forms import WeightsAndMeasurementsForm

@pytest.mark.parametrize(
    "view, model, std_prefix", 
    [
        ('length', Length, 'Foot'),
        ('time', Time, 'Round'), 
        ('weight', Weight, 'Pound')
    ]
)
class TestBlueprint:
    """GIVEN a view for '/mechanics/weights_and_measures/<view> ..."""
    # TODO: form validation with errors

    @pytest.fixture(autouse=True)
    def _setup(self, client, disable_login, request, session, utils, 
        view, model, std_prefix):

        if 'enable_login' in request.keywords:
            current_app.config['LOGIN_DISABLED'] = False

        self.client = client
        self.view = view
        self.model = model
        self.std_prefix = std_prefix
        self.url = url_for('mechanics.wnm.' + view)
        self.utils = utils
        self.request = request

    def insert(self, data=None):
        if not data:
            data = self.data()
        db.session.add(self.model(**data))

    def data(unit='new_unit', desc='The new unit.', std=3.14):
        return dict(unit='new_unit', desc='The new unit.', std=3.14)

    @pytest.mark.enable_login
    @pytest.mark.parametrize(
        "user, status_code",
        [
            (None, 404),
            ('user_player', 404),
            ('user_dm', 404),
            ('user_admin', 200)
        ]
    )
    def test_user_access(self, user, status_code):
        """
        WHEN a request is made
        THEN validate that an 'Admin' is signed in, the url for <view> is
             received, and there are no inital errors.
        """

        # Setup
        if user:
            self.utils.login(self.client, self.request.getfixturevalue(user))

        # Act
        resp = self.client.get(self.url)

        # Assert
        assert resp.status_code == status_code
        assert resp.request.path == self.url
        assert b'error' not in resp.data

    @pytest.mark.parametrize('link', ['home.index', 'mechanics.index'])
    def test_main_links(self, link):
        """
        WHEN a request is made and the user is authenticated
        THEN verify that links are available for the home page and 
             parent blueprint.
        """

        # Setup
        link = url_for(link)

        # Act
        resp = self.client.get(self.url)

        # Assert
        assert link.encode() in resp.data
    
    def test_headers(self):
        """
        WHEN a request is made
        THEN display the data headers.
        """

        # Setup
        headers = ('Unit', 'Description', self.std_prefix + ' Standard')

        # Act
        resp = self.client.get(self.url)

        # Assert
        for _header in headers:
            assert str(_header).encode() in resp.data

    def test_view_data(self):
        """
        WHEN a request is made
        THEN display the data from the database.
        """

        # Setup
        data = self.data()
        self.insert(data)

        # Act
        resp = self.client.get(self.url)

        # Assert
        for key, _data in data.items():
            assert str(_data).encode() in resp.data

    def test_add_data(self):
        """
        WHEN a POST request is made to add data to the database
        THEN add the data to the database.
        """

        # Setup
        data = self.data()

        # Act
        resp = self.client.post(self.url, data=data, follow_redirects=True)

        # Assert
        assert resp.status_code == 200
        assert resp.request.path == self.url
        for key, _data in data.items():
            assert str(_data).encode() in resp.data

    def test_uniqueness(self):
        """
        WHEN a POST request is made to add data to the database that is not unique
        THEN do not add the data to the data and respond with an error message.
        """

        # Setup
        data = self.data()
        self.insert(data)

        # Act
        resp = self.client.post(self.url, data=data, follow_redirects=True)

        # Assert
        assert resp.status_code == 200
        assert resp.request.path == self.url
        for key, _data in data.items():
            assert str(_data).encode() in resp.data
        assert b'error' in resp.data