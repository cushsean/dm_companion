from app.blueprints.mechanics.blueprints.weights_and_measures.models import Length, Time, Weight

def test_lenght_model():
    """
    GIVEN a lenght model
    WHEN a new lenght is created
    THEN check the model's values
    """

    lenght_unit = "new_unit"
    length_desc = "unit_description"
    length_std = 3.75

    length = Length(
        unit = lenght_unit,
        desc = length_desc,
        std = length_std
    )

    assert length.unit == lenght_unit
    assert length.desc == length_desc
    assert length.std == length_std


def test_time_model():
    """
    GIVEN a time model
    WHEN a new time is created
    THEN check the model's values
    """

    time_unit = "new_unit"
    time_desc = "unit_description"
    time_std = 3.75

    time = Time(
        unit = time_unit,
        desc = time_desc,
        std = time_std
    )

    assert time.unit == time_unit
    assert time.desc == time_desc
    assert time.std == time_std


def test_weight_model():
    """
    GIVEN a weight model
    WHEN a new weight is created
    THEN check the model's values
    """

    weight_unit = "new_unit"
    weight_desc = "unit_description"
    weight_std = 3.75

    weight = Weight(
        unit = weight_unit,
        desc = weight_desc,
        std = weight_std
    )

    assert weight.unit == weight_unit
    assert weight.desc == weight_desc
    assert weight.std == weight_std
