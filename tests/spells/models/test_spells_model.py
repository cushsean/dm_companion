from app.blueprints.mechanics.blueprints.weights_and_measures.models import Length, Time
from app.blueprints.spells.models import Spell

def test_spell_model():
    """
    GIVEN a Spell model
    WHEN a new spell is created
    THEN check that its values are created correctly.
    """

    spell_name = "name of spell"
    spell_type = "Evocation"
    spell_comp = "V, S, M, F"
    spell_time = Time()
    spell_range = Length()
    spell_eat = "This is the Effect, Area or Target"
    spell_duration = Length()
    spell_save = "Reflex [half]"
    spell_resistance = True
    spell_description = "This is the description."

    spell = Spell(
        name = spell_name,
        type = spell_type,
        comp = spell_comp,
        time = spell_time,
        range = spell_range,
        eat = spell_eat,
        duration = spell_duration,
        save = spell_save,
        resistance = spell_resistance,
        description = spell_description
    )

    assert spell.name == spell_name
    assert spell.type == spell_type
    assert spell.comp == spell_comp
    assert spell.time == spell_time
    assert spell.range == spell_range
    assert spell.eat == spell_eat
    assert spell.duration == spell_duration
    assert spell.save == spell_save
    assert spell.resistance == spell_resistance
    assert spell.description == spell_description
