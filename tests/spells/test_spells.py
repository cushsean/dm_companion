import pytest

from flask import request, url_for


class TestSpells:
    def test_get(self, client):
        """
        GIVEN a view for '/spells'
        WHEN a GET request is made
        THEN respond with no errors.
        """

        # Setup
        url = url_for('spells.index')

        # Act
        resp = client.get(url)

        # Assert
        assert resp.status_code == 200
        assert resp.request.path == url
        assert b'error' not in resp.data

    def test_get(self, client):
        """
        GIVEN a view for '/spells'
        WHEN a POST request is made
        THEN respond 405 and 'Method Not Allowed'.
        """

        # Setup
        url = url_for('spells.index')

        # Act
        resp = client.post(url)

        # Assert
        assert resp.status_code == 405
        assert resp.request.path == url
        assert b'Method Not Allowed' in resp.data