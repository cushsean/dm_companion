import pytest


def test_home_from_root_status(client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/' page is requested (GET)
    THEN check that the response has status 200 (success)
    """

    response = client.get('/')
    
    assert response.status_code == 200


def test_home_from_home_status(client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/home' page is requested (GET)
    THEN  check that the response has status 200 (success)
    """

    response = client.get('/home')

    assert response.status_code == 200
