from app import db

def test_empty_db(client):
    """
    GIVEN a sqlite database
    WHEN it is initally created
    THEN it should not contain any tables.
    """

    rv = client.get('/')
    with db.engine.connect() as con:
        rs = con.execute('SELECT * FROM sqlite_master WHERE type="table" ORDER BY name;')
        for row in rs:
            assert False
