import pytest

from flask import request, url_for

# from app import db
from app.blueprints.auth.models import PrivilegeLevel, User
from app.blueprints.admin.forms import UserControlForm


class TestPrivilege:
    @pytest.mark.parametrize(
        'user, status_code',
        [
            (None, 404),
            ('user_player', 404),
            ('user_dm', 404),
            ('user_admin', 200)
        ]
    )
    def test_user_access(self, client, utils, request, privilege_admin,
                         user, status_code):
        """
        GIVEN a GET request for 'admin/user'
        WHEN a user attempts to access the view
        THEN respond with 404 if the user is not an admin or is not signed in
             and 200 with no errors if the user is an admin.
        """

        # Setup
        url = url_for('admin.user')

        if user:
            utils.login(client, request.getfixturevalue(user))

        # Act
        resp = client.get(url, follow_redirects=True)

        # Assert
        assert resp.status_code == status_code
        assert resp.request.path == url
        assert b'error' not in resp.data

    def test_get_base_url(self, client, import_users, disable_login):
        """
        GIVEN a GET request for 'admin/user/'
        WHEN a username is not specified i.e. 'admin/user/<username>
        THEN respond with 200, no errors, and a list of all users that 
             are hyperlinks.
        """

        # Setup
        status_code = 200
        url = url_for('admin.user')
        users = []
        for user in import_users:
            users.append(user['username'])

        # Act
        resp = client.get(url, follow_redirects=True)

        # Assert
        assert resp.status_code == status_code
        assert b'error' not in resp.data
        for username in users:
            assert (username + "</a>").encode() in resp.data
            
    def test_get_invalid_username(self, client, session, disable_login):
        """
        GIVEN a GET request for 'admin/user/<username>'
        WHEN the user does not exist
        THEN respond with 'admin/user', code 200, and error message
        """

        # Setup
        status_code = 200
        username = 'username'
        url_get = url_for('admin.user', username=username)
        url_resp = url_for('admin.user')

        # Act
        resp = client.get(url_get, follow_redirects=True)

        # Assert
        assert resp.status_code == status_code
        assert resp.request.path == url_resp
        assert b'error' in resp.data

    def test_get_username(self, client, session, user_player,
                          disable_login, utils):
        """
        GIVEN a GET request for 'admin/user/<username>'
        WHEN the user exists
        THEN respond with 'admin/user/<username>', code 200, no errors, and 
             display the users information but not their password or hashword.
        """

        # Setup
        status_code = 200
        url = url_for('admin.user', username=user_player['username'])
        user = utils.get_user(user_player)
        user_disabled = User.query.filter_by(username=user.username).first().disabled

        # Act
        resp = client.get(url, follow_redirects=True)

        # Assert
        assert resp.status_code == status_code
        assert b'error' not in resp.data
        assert resp.request.path == url
        assert user.username.encode() in resp.data
        assert user.email.encode() in resp.data
        assert user.password not in resp.data
        assert user_player['password'].encode() not in resp.data
        assert user.date_joined.strftime("%d %b %Y").encode() in resp.data
        assert ('Privilege Level: ' + user_player['privilege'].name).encode() in resp.data
        if user_disabled:
            assert b'input checked id="disabled"' in resp.data
        else:
            assert b'input id="disabled"' in resp.data

    @pytest.mark.xfail(reason=('user.py actual does not support changing' \
                               ' privilege level at this time. Have not been' \
                               ' able to verify successful functionality.'))
    def test_post_change_privilege(self, client, session, user_player,
                                   disable_login, utils):
        """
        GIVEN a POST request for 'admin/user/<username>'
        WHEN the user exists and their privilege level is changed
        THEN respond with 'admin/user/<username>', code 200, no errors, and 
             display the users information reflecting the change but not their 
             password or hashword.
        """

        # Setup
        status_code = 200
        url = url_for('admin.user', username=user_player['username'])
        user = utils.get_user(user_player)
        user_disabled = User.query.filter_by(username=user.username).first().disabled
        new_privilege = 'Admin'

        data = dict(
            privilege = new_privilege
        )

        # Act
        resp = client.post(url, data=data, follow_redirects=True)

        # Assert
        assert resp.status_code == status_code
        assert b'error' not in resp.data
        assert resp.request.path == url
        assert user.username.encode() in resp.data
        assert user.email.encode() in resp.data
        assert user.password not in resp.data
        assert user_player['password'].encode() not in resp.data
        assert user.date_joined.strftime("%d %b %Y").encode() in resp.data
        assert ('Privilege Level: ' + new_privilege).encode() in resp.data
        if user_disabled:
            assert b'input checked id="disabled"' in resp.data
        else:
            assert b'input id="disabled"' in resp.data

    def test_post_change_disabled(self, client, session, user_player,
                          disable_login, utils):
        """
        GIVEN a POST request for 'admin/user/<username>'
        WHEN the user exists and their disabled status is changed
        THEN respond with 'admin/user/<username>', code 200, no errors, and 
             display the users information reflecting the change but not their 
             password or hashword.
        """

        # Setup
        status_code = 200
        url = url_for('admin.user', username=user_player['username'])
        user = utils.get_user(user_player)
        user_disabled = not User.query.filter_by(username=user.username).first().disabled

        data = dict(
            disabled = user_disabled
        )

        # Act
        resp = client.post(url, data=data, follow_redirects=True)

        # Assert
        assert resp.status_code == status_code
        assert b'error' not in resp.data
        assert resp.request.path == url
        assert user.username.encode() in resp.data
        assert user.email.encode() in resp.data
        assert user.password not in resp.data
        assert user_player['password'].encode() not in resp.data
        assert user.date_joined.strftime("%d %b %Y").encode() in resp.data
        assert ('Privilege Level: ' + user_player['privilege'].name).encode() in resp.data
        if user_disabled:
            assert b'input checked id="disabled"' in resp.data
        else:
            assert b'input id="disabled"' in resp.data