import pytest

from flask import request, url_for
from sqlalchemy.exc import IntegrityError

# from app import db
from app.blueprints.auth.models import PrivilegeLevel


class TestPrivilege:
    def test_get_not_logged_in(self, client):
        """
        GIVEN a get request for '/admin/privilege'
        WHEN a user is not logged in
        THEN respond with 404
        """

        # Setup
        status_code = 404
        url = url_for('admin.privilege')

        # Act
        resp = client.get(url, follow_redirects=True)

        # Assert
        assert resp.status_code == status_code
        assert resp.request.path == url

    @pytest.mark.parametrize(
        'user, status_code, url_resp',
        [
            ('user_player', 404, 'admin.privilege'),
            ('user_dm', 404, 'admin.privilege'),
            ('user_admin', 200, 'admin.privilege')
        ]
    )
    def test_get_logged_in(self,
                           client,
                           request,
                           utils,
                           privilege_admin,
                           user,
                           status_code,
                           url_resp):
        """
        GIVEN a get request for '/admin/privilege'
        WHEN a user is logged in
        THEN respond with 404 if not admin
        """

        # Setup
        url_get = url_for('admin.privilege')
        utils.login(client, request.getfixturevalue(user))

        # Act
        resp = client.get(url_get, follow_redirects=True)

        # Assert
        assert resp.status_code == status_code
        assert resp.request.path == url_for(url_resp)

    @pytest.mark.xfail(reason='Setting privilege level not currently supported.')
    def test_add_new_privilege(self, client, utils, user_admin):
        """
        GIVEN a post request for '/admin/privilege'
        WHEN a user submits the form for a new privilege
        THEN respond with 200 and add the privilege to the database.
        """

        # Setup
        status_code = 200
        url = url_for('admin.privilege')
        utils.login(client, user_admin)
        privilege = 'new_privilege'
        level = 3

        data = dict(
            privilege = privilege,
            level = level
        )

        # Act
        resp = client.post(url, data=data, follow_redirects=True)

        # Assert
        assert resp.status_code == status_code
        assert resp.request.path == url
        assert b'error' not in resp.data
        results = PrivilegeLevel.query.filter_by(name=privilege).first()
        assert privilege == results.name
        assert level == results.level

    @pytest.mark.parametrize(
        'first_name, second_name, first_level, second_level, allowed',
        [
            ('new_name', 'new_name', 10, 10, False),
            ('new_name', 'diff_name', 10, 10, True),
            ('new_name', 'diff_name', 10, 9, True),
            ('new_name', 'new_name', 10, 9, False)
        ]
    )
    def test_uniqueness(self, client, utils, user_admin, first_name,
                        second_name, first_level, second_level, allowed):
        """
        GIVEN a post request for 'admin/privilege'
        WHEN a new privilege is created
        THEN only add to database if it has a unique name, 
             otherwise, throw IntegrityError, do not add new privilege to
             database
        """

        # Setup
        status_code = 200
        utils.login(client, user_admin)
        url = url_for('admin.privilege')

        first_data = dict(
            privilege = first_name,
            level = first_level
        )

        second_data = dict(
            privilege = second_name,
            level = second_level
        )

        # First add
        resp = client.post(url, data=first_data, follow_redirects=True)
        count = 0 # Rollback with destroy all
        error = False
        if allowed:
            count = 3 # Admin and two additionals
        else:
            error = True
        
        # Act
        resp = client.post(url, data=second_data, follow_redirects=True)

        # Assert
        assert resp.status_code == status_code
        assert resp.request.path == url
        assert count == PrivilegeLevel.query.count()
        if error:
            assert b'error' in resp.data
        else:
            assert b'error' not in resp.data


