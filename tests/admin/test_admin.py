import pytest

from flask import request, url_for

class TestAdmin:
    @pytest.mark.parametrize('user', ['user_player', 'user_dm'])
    def test_non_admin_access(self, client, utils, request, user, privilege_admin):
        """
        GIVEN a GET request to '/admin'
        WHEN a non admin trys to access the blueprint
        THEN return 404
        """
        
        # Setup
        status_code = 404
        url_get = url_for('admin.index')
        _user = request.getfixturevalue(user)
        utils.login(client, _user)

        # Act
        resp = client.get(url_get, follow_redirects=True)

        # Assert
        assert resp.status_code == status_code

    def test_admin_access(self, client, utils, user_admin):
        """
        GIVEN a GET request to '/admin'
        WHEN the logged in user is admin
        THEN respond with code 200 and no errors
        """

        # Setup
        status_code = 200
        url = url_for('admin.index')
        utils.login(client, user_admin)

        # Act
        resp = client.get(url, follow_redirects=True)

        # Assert
        assert resp.status_code == status_code
        assert b'error' not in resp.data

    def test_admin_access(self, client, utils, user_admin):
        """
        GIVEN a POST request to '/admin'
        WHEN the logged in user is admin
        THEN respond with code 405.
        """

        # Setup
        status_code = 405
        url = url_for('admin.index')
        utils.login(client, user_admin)

        # Act
        resp = client.post(url, follow_redirects=True)

        # Assert
        assert resp.status_code == status_code

    def test_no_login(self, client, privilege_admin):
        """
        GIVEN a GET request to '/admin'
        WHEN there is no user logged in
        THEN respond with 404
        """
        
        # Setup
        status_code = 404
        url = url_for('admin.index')

        # Act
        resp = client.get(url, follow_redirects=True)

        # Assert
        assert resp.status_code == status_code
