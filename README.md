# DM_Companion

This companion application is intended to be a tool used by Dungeon Masters (DM) to help them manage sessions with less effort without sacrificing creativeness.

# Version

0.0.1

# Quick Start

### INSTALL

Setup a venv with the following:
1. python version 3.6.9
2. env FLASK_APP=app
3. env FLASK_ENV=development *USED FOR DEVELOPMENT ONLY*

Create an instance configuration file at: dm_compainion/instance/config.py
Include the following:
1. SECRET_KEY = '<your_secret_key>'
2. MAIN_FROM_EMAIL = "<your_email>"

pip install -r requirements.txt

### D&D Verison Notes

Spreadsheet: https://docs.google.com/spreadsheets/d/1vtbVWeF7l51dPmGoyLaxorIwixbBKmFQ7UgPTLBYa6o/edit#gid=914542804

Word Docs: https://drive.google.com/drive/folders/1IWFBx22h059jbTyNyA7AF-T1XQ6Wc8sK?usp=sharing

### LAUCH SERVER

flask run

# Requirements

python 3.6.9

# Contributers

- Sean Cushman "cushsean"
- Cody Larkin "keeper317"

# Documentation
All functions should have a DocString explaining, at a minimum, why they were created.
If the function renders a view, the DocString should follow this format -
"""
Why does the view exist?

Access:
READ  - 1  (Admin)
WRTIE - 5  (DM)
EXEC  - 10 (PLAYER)
`Exclude any that are not used.
`Include the Privilege Value followed by the associated name in parenthesis.
`The READ, WRITE, and EXEC should be indented four spaces.

The user can perform the following actions:
- View this
- Edit that
- TODO: Delete that thing
`Every action should be tested for.
`Actions that are in progress or not started should begin with TODO:

Any additional information for the developer can go here.
"""

By using the above DocString, developers can better prepare tests up front, allowing for more honest testing of the view. In addition, it allows developers to see exactly what should be happening in the view as well as what still needs worked on.

# Privilege Levels
This application assigns privilege levels to users to define their individual permission level.
When a user is created they are PLAYERs.
ADMINS can change users permissions at will.

### Admin
Admins have full control of the website.
Key abilities include:
- Disable/Enable user accounts
- Change accoutn privilege levels
- Add additional privilege levels

### DM
DMs have access to things that players don't need to concern themselfs with.

### Player
Players, the default privilege level, have access to their own account controls.
Key abilities include:
- Disable their own account
- Change Username/Password/Email

# Blueprints

## What is a Blueprint?
A Blueprint defines the different areas of the website.
Each Blueprint has its own prefix url.

### Admin
PRIVILEGE LEVEL: Admin

From here Admins can modify user accounts and adjust privilege levels.

### Auth
PRIVILEGE LEVEL: N/A

This handels all the user sign-in/out and join mechanics.

### Economy
PRIVILEGE LEVEL: DM

This manages the economic systems.

### Home
PRIVILEGE LEVEL: N/A

This is the home page.

### User
PRIVILEGE LEVEL: Player

This is where users can view and modify their own information.

# Database Tabels

## user
Manages user profiles.

### id
Primary Key.

### username
User's username.

Requirments:
- Between 5 and 32 characters.

### email
User's email.

Requirements:
- Must be email formate.

### password
User's hashed password.

Requirements:
- Between 8 and 255 characters.

### date_joined
The date the user joined.

### privilege_id
The foreign key of the user's privilege level.

### disabled
Boolean. True = Account Disabled; False = Account Enabled.

## privilege_level
List of privilege levels.

### id
Primary Key

### name
Name of the privilege level.
