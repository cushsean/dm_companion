from flask import flash

class errors:
    """
    Adds the ability to track the number of messages added to the
    Flask flash protocal.

    Anywhere you would normally call flash(), instead call errors.push().
    Calling errors.push(None) will not add a message to flash or increase count.

    Call errors.count to get the current count.

    Call get_flashed_messages() as normal.
    """

    def __init__(self):
        self.count = 0

    def push(self, msg: str = None) -> None:
        """Pushes an error message to the flash stack."""
        if msg:
            flash(msg, 'error')
            self.count += 1

        return

    def count(self) -> int:
        """Returns the number of messages pushed to flash."""
        return self.count
