from sqlalchemy.orm import relationship

from app import db

class SpellModel(db.Model):
    __tablename__ = 'spells'

    # Columns
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(64), unique=True)
    typ = db.Column(db.String(64))
    comp = db.Column(db.String(64))
    time = db.Column(db.String(32))
    rng = db.Column(db.String(32))
    eta = db.Column(db.String(128))
    duration = db.Column(db.String(32))
    save = db.Column(db.String(32))
    resistance = db.Column(db.String(128))
    level = db.Column(db.String(64))
    mana = db.Column(db.Integer)
    desc = db.Column(db.Text())
    wiki = db.Column(db.String(128))

