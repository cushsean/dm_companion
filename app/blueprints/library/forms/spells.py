from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, IntegerField, HiddenField, Form, FieldList, FormField

class SpellSubForm(Form):

	name = StringField()
	typ = StringField()
	comp = StringField()
	time = StringField()
	rng = StringField()
	eta = StringField()
	duration = StringField()
	save = StringField()
	resistance = StringField()
	level = StringField()
	mana = IntegerField()
	desc = StringField()
	wiki = StringField()
	edit = SubmitField()
	remove = SubmitField()
	id = HiddenField()

	class meta:
		csrf = False


class SpellForm(FlaskForm):
	subform = FieldList(FormField(SpellSubForm))
	add_spell = SubmitField(label='Add Spell')