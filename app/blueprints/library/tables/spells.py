from flask_table import Table, Col, LinkCol, ButtonCol

class SpellTable(Table):
	name = Col('Name')
	typ = Col('Type')
	comp = Col('Component')
	time = Col('Time')
	rng = Col('Range')
	eta = Col('Effect/Area/Target')
	duration = Col('Duration')
	save = Col('Save')
	resistance = Col('Resistance')
	level = Col('Level')
	mana = Col('Mana')
	desc = Col('Description')
	wiki = Col('Wiki')
	edit = Col('')
	remove = Col('')
	id = Col('')

class SpellItem(object):
	def __init__(self, name, typ, comp, time, rng, eta, duration, save, resistance, level, mana, desc, wiki, edit, id, remove):
		self.name = name
		self.typ = typ
		self.comp = comp
		self.time = time
		self.rng = rng
		self.eta = eta
		self.duration = duration
		self.save = save
		self.resistance = resistance
		self.level = level
		self.mana = mana
		self.desc = desc
		self.wiki = wiki
		self.edit = edit
		self.model_id = model_id
		self.remove = remove