from flask import abort, Blueprint, render_template

from .models import *

bp = Blueprint('library', __name__, url_prefix='/library', template_folder='templates')

@bp.route('')
def index():
    return render_template('library/index.html')

from .views.spells import spells

bp.add_url_rule('spells', view_func=spells, methods=["GET", "POST"])