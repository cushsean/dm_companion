from flask import render_template, request

from app.blueprints.library.forms import SpellForm
from app.blueprints.library.models import SpellModel
from app.blueprints.library.tables import SpellItem, SpellTable

def spells():

	form = SpellForm()
	table = spellsList(form)

	if request.method == 'POST':
		# print(request.form)
		# spellsAdd()
		pass
	return render_template('library/list.html', table=table, form=form)


def spellsList(form):
	if request.method == 'GET':
		for record in SpellModel.query.all():
			form.subform.append_entry(record)

	table = SpellTable(form.subform)
	print(table)
	return table


# def spellsAdd():
# 	print('check')
# 	return