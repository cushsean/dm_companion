from flask import redirect, render_template, url_for

from sqlalchemy.exc import IntegrityError

from app.blueprints.economy.forms import CurrencyForm
from app.blueprints.economy.models import Currency
from app.helpers import errors

from app import db

def currency():
    """
    View and add currency data to the database.

    Access:
        READ:  10 (Player)
        WRITE: 5  (DM)

    Actions:
        Players:
            - View existing currencies in the database to include their Name, 
                Description, and Gold Standard.
        DM:
            - TODO: Edit existing currencies.
            - TODO: Delete existing currencies.
            - Add new currencies.
    """

    err = errors()
    form = CurrencyForm()

    __currency = db.session.query(Currency).all()
    _currency = []
    for curr in __currency:
        _currency.append(
            {
                'item': curr.item,
                'desc': curr.description,
                'gold_std': curr.gold_std
            }
        )

    if form.validate_on_submit():
        new_currency = Currency(
            item = form.item.data,
            description = form.desc.data,
            gold_std = form.gold_std.data
        )

        db.session.add(new_currency)

        try:
            db.session.commit()
            return redirect(url_for('economy.currency'))
        except IntegrityError:
            db.session.rollback()
            err.push('Currency already exists.')

    return render_template('economy/currency.html', form=form, curr=_currency)