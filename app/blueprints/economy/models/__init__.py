from sqlalchemy.orm import relationship

from app import db

class Currency(db.Model):
    __tablename__ = 'currency'

    # Columns
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    item = db.Column(db.String(32), unique=True, nullable=False)
    description = db.Column('discription', db.Text())
    gold_std = db.Column(db.Float())
