from flask_wtf import FlaskForm
from wtforms import DecimalField, StringField, SubmitField
from wtforms.validators import InputRequired

class CurrencyForm(FlaskForm):
    item = StringField(label='Currency', validators=[InputRequired()])
    desc = StringField(label='Description', validators=[InputRequired()])
    gold_std = DecimalField(validators=[InputRequired()])
    add = SubmitField('Add')

