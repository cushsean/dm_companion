from flask import abort, Blueprint, render_template

from .models import *
from app.blueprints.auth import is_dm


bp = Blueprint('economy', __name__, url_prefix='/economy', template_folder='templates')

@bp.before_request
def restrict_bp_to_dm():
    """Only allows DMs in this blueprint."""
    if not is_dm():
        abort(404)

@bp.route('')
def index():
    """
    Index for economy blueprint.

    Access:
        With the page permission method of access. All users will have access
        to this index page.

    Actions:
        Users can select from the list of sub directories.
    """
    return render_template('economy/index.html')

from .views.currency import currency

bp.add_url_rule('/currency', view_func=currency, methods=["GET", "POST"])