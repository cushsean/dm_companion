from flask import Blueprint, redirect, url_for
from flask_login import login_required

from .views.profile import profile

bp = Blueprint('user', __name__, url_prefix='/user', template_folder='templates')


@bp.route('/')
@login_required
def index():
    return redirect(url_for('user.profile'))

bp.add_url_rule('/profile', view_func=profile, methods=["GET", "POST"])
