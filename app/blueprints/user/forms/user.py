from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import Email, Length, InputRequired, EqualTo

from flask_login import current_user

from app.blueprints.auth.forms import (
    PASSWORD_VALIDATORS,
    USERNAME_VALIDATORS,
    EMAIL_VALIDATORS
)


class UserProfileForm(FlaskForm):
    """Display and update the user's profile."""

    username = StringField(
        label='Username:',
        validators=USERNAME_VALIDATORS
    )

    email = StringField(
        label='Email:',
        validators=EMAIL_VALIDATORS
    )

    password = SubmitField('Change Password')

    delete = SubmitField('Delete Account')

    reset = SubmitField('Reset')

    update = SubmitField('Update Profile')
