from flask import Blueprint, render_template, redirect, url_for

from flask_login import login_required, current_user

from sqlalchemy.exc import IntegrityError

from app import db
from app.blueprints.auth.models import User
from app.blueprints.user.forms.user import UserProfileForm
from app.helpers import errors

@login_required
def profile(user=current_user):
    """The user's profile page.

    The user can edit the following information.

    1. Change username.
    2. Change email.
    3. Change password (new view)
    4. Delete account.
    """

    err = errors()
    form = UserProfileForm()

    # Save for reset
    username = user.username
    email = user.email

    # Change password flag
    change_password = False

    if form.validate_on_submit():
        if not user.username == form.username.data:
            user.username = form.username.data

        if not user.email == form.email.data:
            user.email = form.email.data

        if form.password.data:
            change_password = True

        if form.delete.data:
            user.disabled = True
            return redirect(url_for('auth.sign_out'))

        if form.reset.data:
            user.username = username
            user.email = email
            return render_template('user/profile.html', form=form, user=user)
        else:
            try:
                db.session.commit()
            except IntegrityError:
                db.session.rollback()
                err.push('Username unavailable.')

        if not form.errors and change_password:
            return redirect(url_for('auth.change_password'))

    return render_template('user/profile.html', form=form, user=user)
