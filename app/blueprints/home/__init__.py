from flask import Blueprint, render_template
from flask_login import current_user

from app.blueprints.auth import is_admin, is_dm

bp = Blueprint('home', __name__, url_prefix='/home', template_folder='templates')

@bp.route('')
def index(user=None, admin=False, dm=False):
    """
    The HOME page for the website.

    Access:
        Everyone

    Currently DM and ADMIN only links are only visible to their respective
        tiers. When the page permissions are implemented the links will be
        available to everyone who has read access.
    """

    if current_user.is_authenticated:
        user=current_user
    if is_admin():
        admin=True
        dm=True
    elif is_dm():
        dm=True

    return render_template('home/index.html', user=user, admin=admin, dm=dm)