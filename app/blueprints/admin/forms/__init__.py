from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, RadioField, BooleanField
from wtforms.fields.html5 import SearchField
from wtforms.validators import InputRequired, Optional

from app import db
from app.blueprints.auth.models import PrivilegeLevel

class PrivilegeForm(FlaskForm):
    """Adds and Modifies Privilege Levels."""

    privilege = StringField(label='Privilege', validators=[InputRequired()])
    submit = SubmitField('Add New Privilege')

class UserControlForm(FlaskForm):
    """Allows admins to control user's privileges."""

    choices = []
    try:
        levels = db.session.query(PrivilegeLevel.name).all()
        for level, in levels:
            choices.append(level)
    except:
        pass

    privilege = RadioField(label='New Privilege Level:', choices=choices, validators=[Optional()])

    disabled = BooleanField(label='Account Disabled:')
    update = SubmitField('Update User')

class SearchUserForm(FlaskForm):
    """Search Box for usernames"""

    search = SearchField(label='Filter Users:')
    submit = SubmitField('Search')
