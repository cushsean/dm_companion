from flask import abort, Blueprint, render_template

from app.blueprints.auth import is_admin


bp = Blueprint('admin', __name__, url_prefix='/admin', template_folder='templates')

@bp.before_request
def restrict_bp_to_admins():
    """Only allows admins in this blueprint."""
    if not is_admin():
        abort(404)

@bp.route('/', methods=["GET"])
def index(character=None):
    return render_template('admin/index.html')

from .views.privilege import privilege
from .views.user import user

bp.add_url_rule('/privilege', view_func=privilege, methods=["GET", "POST"])
bp.add_url_rule('/user', view_func=user, methods=["GET", "POST"])
bp.add_url_rule('/user/<username>', view_func=user, methods=["GET", "POST"])
