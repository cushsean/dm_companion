from flask import render_template
from sqlalchemy.exc import IntegrityError

from app import db
from app.blueprints.auth.models import PrivilegeLevel
from app.blueprints.admin.forms import PrivilegeForm
from app.helpers import errors


def privilege():
    """
    This view will allow Admins to add new privileges with an associated level
    that can be assigned to users. 
    
    Access:
        READ  - 1 (ADMIN)
        WRITE - 1 (ADMIN)

    The user should be able to preform the following actions:
        - View the existing privileges
        - TODO: Edit privilege names 
        - TODO: Edit privilege level values
        - TODO: Delete privileges
        - Create new privileges
            TODO: Set privilege level
        - TODO: See how many users and views are assigned to each privilege
    
    When a privilege is deleted, all users assigned to that privilege will be 
    downgraded to the next level lower than the level they were 
    previously assigned. All views with be upgraded to the next privilege higher
    that the level they were previously assigned.

    The statement above should be presented to the user as a warning when the 
    user attempts to remove a privilege, and a confirmation should be received.
    """

    err = errors()
    form = PrivilegeForm()
    levels = []
    results = db.session.query(PrivilegeLevel.name).all()
    # TODO: Avoid unpacking query results.
    for value, in results:
        levels.append(value)

    if form.validate_on_submit():
        new_privilege = PrivilegeLevel(name=form.privilege.data)

        db.session.add(new_privilege)
        try:
            db.session.commit()
            levels.append(form.privilege.data)
            form.privilege.data = ""
        except IntegrityError:
            db.session.rollback()
            err.push('Privilege already exists.')

    return render_template('admin/privilege.html', form=form, levels=levels)