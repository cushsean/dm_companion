from flask import render_template, redirect, url_for
from sqlalchemy.exc import IntegrityError

from app import db
from app.blueprints.auth.models import PrivilegeLevel, User
from app.blueprints.admin.forms import PrivilegeForm, UserControlForm
from app.helpers import errors


def user(username=None, form=None):
    """
    This view will allow Admins to view and modify all user's profiles.

    Access: 
        READ  - 1 (ADMIN)
        WRITE - 1 (ADMIN)

    The user should be able to preform the following actions:
        - View the username
        - View the date joined
        - View the email
        - View the privilege level
        - Modify the privilege level
        - Disable the account

    NOTE: The user should not have access to the user's password, even as a 
          hashed string.
    """

    err = errors()

    if username:
        # User selected
            # Display user profile
        user = User.query.filter_by(username=username).first()
        if not user:
            # User was not found
            err.push('User ' + username + ' was not found.')
            return redirect(url_for('admin.user'))

        # levels = db.session.query(PrivilegeLevel.name).all()
        # choices = []
        # for level, in levels:
        #     choices.append(level)

        form = UserControlForm()

        if form.validate_on_submit():
            # Allow admin to change user privilege level
            if form.privilege.data:
                privilege_id = PrivilegeLevel.query.filter_by(name=form.privilege.data).first()
                user.privilege = privilege_id

            # Allow admin to disable the users account
            user.disabled = form.disabled.data

            try:
                db.session.commit()
            except: # pragma: no cover
                db.session.rollback()
                err.push('Something went wrong.')

        return render_template('admin/user.html', form=form, user=user)

    # No user selected
        # TODO: Privide Search/Filter to narrow down list
    user_list = db.session.query(User.username).all()
    username_list = []
    for username, in user_list:
        username_list.append(username)

    return render_template('admin/user.html', username_list=username_list)
