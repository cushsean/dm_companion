from flask_wtf import FlaskForm
from wtforms import DecimalField, StringField, SubmitField
from wtforms.validators import InputRequired


class DamageTypeForm(FlaskForm):
    """For adding and editing damage types."""

    dmg_type = StringField(label='Type', validators=[InputRequired()])
    dmg_desc = StringField(label='Description')
    dmg_example = StringField(label='Example')
    add = SubmitField('Add')

