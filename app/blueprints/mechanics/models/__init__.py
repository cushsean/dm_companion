from sqlalchemy.orm import relationship

from app import db


class DamageType(db.Model):
    __tablename__ = 'damage_type'

    # Columns
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    dmg_type = db.Column(db.String(64), unique=True, nullable=False)
    dmg_description = db.Column('dmg_discription', db.Text())
    dmg_example = db.Column(db.Text())

