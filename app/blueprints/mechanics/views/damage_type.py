from flask import redirect, render_template, url_for
from sqlalchemy.exc import IntegrityError

from app import db
from app.blueprints.mechanics.forms import DamageTypeForm
from app.blueprints.mechanics.models import DamageType
from app.helpers import errors


def damage_type():

    dmg_types=[]
    err = errors()
    form = DamageTypeForm()

    results = db.session.query(DamageType).all()
    for dmg in results:
        dmg_types.append(
            {
                'name': dmg.dmg_type,
                'desc': dmg.dmg_description,
                'example': dmg.dmg_example
            }
        )

    if form.validate_on_submit():
        new_dmg = DamageType(
            dmg_type = form.dmg_type.data,
            dmg_description = form.dmg_desc.data,
            dmg_example = form.dmg_example.data
        )

        db.session.add(new_dmg)

        try:
            db.session.commit()
            return redirect(url_for('mechanics.damage_type', dmg_types=[]))
        except IntegrityError:
            db.session.rollback()
            err.push('Damage Type already exists.')

    return render_template('mechanics/damage_type.html', form=form, dmg_types=dmg_types)