from flask import abort, Blueprint, render_template

from .blueprints import weights_and_measures
from .models import *
from app.blueprints.auth import is_admin


bp = Blueprint('mechanics', __name__, url_prefix='/mechanics', template_folder='templates')
bp.register_blueprint(weights_and_measures.wnm_bp)

@bp.before_request
def restrict_bp_to_admin():
    """Only allows Admins in this blueprint."""
    if not is_admin():
        abort(404)

@bp.route('')
def index(errors=[]):
    return render_template('mechanics/index.html', errors=errors)


from .views.damage_type import damage_type

bp.add_url_rule('/damage_type', view_func=damage_type, methods=["GET", "POST"])