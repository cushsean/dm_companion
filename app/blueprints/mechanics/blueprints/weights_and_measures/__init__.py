from flask import Blueprint, render_template

from .models import *

wnm_bp = Blueprint('wnm', __name__, url_prefix='/weights_and_measures', template_folder='templates')

from .views.weight import weight
from .views.length import length
from .views.time import time

wnm_bp.add_url_rule('/weight', view_func=weight, methods=["GET", "POST"])
wnm_bp.add_url_rule('/length', view_func=length, methods=["GET", "POST"])
wnm_bp.add_url_rule('/time', view_func=time, methods=["GET", "POST"])
