from flask import redirect, render_template, url_for
from sqlalchemy.exc import IntegrityError

from app import db
from app.blueprints.mechanics.blueprints.weights_and_measures.forms import WeightsAndMeasurementsForm
from app.blueprints.mechanics.blueprints.weights_and_measures.models import Time as Model
from app.helpers import errors


def time():
    err = errors()
    form = WeightsAndMeasurementsForm()
    unit_list=[]

    results = db.session.query(Model).all()
    for _unit in results:
        unit_list.append(
            {
                'unit': _unit.unit,
                'desc': _unit.desc,
                'std': _unit.std
            }
        )

    if form.validate_on_submit():
        new_unit = Model(
            unit = form.unit.data,
            desc = form.desc.data,
            std = form.std.data
        )

        db.session.add(new_unit)

        try:
            db.session.commit()
            return redirect(url_for('mechanics.wnm.time'))
        except IntegrityError:
            db.session.rollback()
            err.push('Unit of Measure already exists.')

    return render_template('wnm/time.html', form=form, unit_list=unit_list)