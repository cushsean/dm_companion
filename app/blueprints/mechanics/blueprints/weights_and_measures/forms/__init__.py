from flask_wtf import FlaskForm
from wtforms import DecimalField, StringField, SubmitField
from wtforms.validators import InputRequired


class WeightsAndMeasurementsForm(FlaskForm):
    unit = StringField(label='Unit of Measure', validators=[InputRequired()])
    desc = StringField(label='Description')
    std = DecimalField(validators=[InputRequired()])
    add = SubmitField('Add')

