from sqlalchemy.orm import relationship

from app import db


class Weight(db.Model):
    # Intended that all object names are consistant
    # Description columns are named 'disc' for lagasy reasons but mostly because 
    #   I'm a dumbass. Please correct when new tables are established --CUSH

    __tablename__ = 'units_of_weight'

    # Columns
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    unit = db.Column(db.String(64), unique=True, nullable=False)
    desc = db.Column('disc', db.Text())
    std = db.Column('lbs_std', db.Float())


class Length(db.Model):
    __tablename__ = 'units_of_length'

    # Columns
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    unit = db.Column(db.String(64), unique=True, nullable=False)
    desc = db.Column('disc', db.Text())
    std = db.Column('ft_std', db.Float())


class Time(db.Model):
    __tablename__ = 'units_of_time'

    # Columns
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    unit = db.Column(db.String(64), unique=True, nullable=False)
    desc = db.Column('disc', db.Text())
    std = db.Column('round_std', db.Float())

