from flask import render_template, redirect, url_for

from sqlalchemy.exc import IntegrityError

from app import db
from app.blueprints.auth import login_attempt
from app.blueprints.auth.forms.join import JoinForm
from app.blueprints.auth.models import User, PrivilegeLevel
from app.helpers import errors


def join():
    """
    Signs a user up with a new account.

    Access:
        No access restrictions.

    Actions:
        - Create a new account.
        - Account must have unique username
        - Account must have unique email
        - Account's email and confirm email must match
        - Account's password and confirm password must match

    Confirmation errors are handled by the form.
    If the username or email already exists, an error is handled here to 
        include a link to auth.signin
    """

    err = errors()
    form = JoinForm()

    if form.validate_on_submit():
        # TODO: Password Validation
        user = User(
            username=form.username.data,
            email=form.email.data,
            password=form.password.data
        )

        # Check if first user
        if not User.query.all():
            user.privilege_id = PrivilegeLevel.query.filter_by(level=1).first().id
            # The first user to be created will be an admin.

        db.session.add(user)
        try:
            db.session.commit()

            # Log new user in and return to index.
            error = login_attempt(form.username.data, form.password.data)
            err.push(error)

            if err.count == 0:      
                return redirect(url_for('home.index'))

        except IntegrityError:
            db.session.rollback()
            err.push(
                'Username or Email already exists.' +
                'Try <a href="' + url_for('auth.sign_in') + '">Signing In</a>.'
            )

    return render_template('auth/join.html', form=form)
