from flask import redirect, url_for
from flask_login import login_required, logout_user

from app import db


@login_required
def sign_out():
    """
    Signs out the current user.

    Access:
        Requires user to be logged in -> redirect to url_for(home.index)

    No action required from user. On success, redirects to url_for(home.index)
    """
    logout_user()
    return redirect(url_for('home.index'))
