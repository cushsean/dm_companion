from flask import render_template, redirect, url_for
from flask_login import current_user, login_required

from app import db
from app.blueprints.auth.forms.update_password import UpdatePasswordForm
from app.helpers import errors


@login_required
def change_password():
    """
    Used to change a user's password.

    Access:
        Requires login

    Actions:
        - User provides current password
        - User provides new password and confirms new password

    If all the fields validate, user's password is changed in the database and
        user is redirected to their profile.
    """

    err = errors()
    form = UpdatePasswordForm()
    
    if form.validate_on_submit():
        if current_user.validate_password(form.current_password.data):
            current_user.password = form.new_password.data
            try:
                db.session.commit()
                return redirect(url_for('user.profile'))
            except: # pragma: no cover
                db.session.rollback()
                err.push('Something went wronge. Changes not made.')

        else:
            err.push('Incorrect Password.')

    return render_template('auth/change_password.html', form=form)
