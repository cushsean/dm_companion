from datetime import datetime

from flask_login import UserMixin

from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship

from app import bcrypt, db
from app.blueprints.characters.models import Character

class User(db.Model, UserMixin):

    __tablename__='user'

    # Columns
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(32), unique=True, nullable=False)
    email = db.Column(db.String(255), unique=True, nullable=False)
    _password = db.Column('password', db.String(255))
    _date_joined = db.Column('date_joined', db.DateTime, default=datetime.now())
    privilege_id = db.Column('privilege_id', db.Integer, db.ForeignKey('privilege_level.id'), default=1)
    disabled = db.Column('disabled', db.Boolean, default=False)
    characters = db.relationship("Character", backref="player")

    # Password hashing system
    @hybrid_property
    def password(self):
        return self._password

    @password.setter
    def password(self, plaintext):
        self._password = bcrypt.generate_password_hash(plaintext)

    # Set default values of properties not set by creator
    @hybrid_property
    def date_joined(self):
        return self._date_joined

    @date_joined.setter
    def date_joined(self, _):
        self._date_joined = datetime.now()

    # Password checker
    def validate_password(self, password):
        return bcrypt.check_password_hash(self._password, password)

class PrivilegeLevel(db.Model):
    __tablename__='privilege_level'

    # Columns
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(
        db.String(32), unique=True, nullable=False
    )
    level = db.Column(
        db.Integer, unique=False, default=10
    )

    privilege = relationship("User", backref='privilege')

class PagePermissions(db.Model):
    __tablename__='page_permissions'

    # Columns
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    page = db.Column(
        db.String(32), unique=True, nullable=False
    )
    read_level = db.Column(
        db.Integer, nullable=False, default=1
    )
    write_level = db.Column(
        db.Integer, nullable=False, default=1
    )