from flask_wtf import FlaskForm
from wtforms import PasswordField, StringField
from wtforms.validators import EqualTo

from app.blueprints.auth.forms import EMAIL_VALIDATORS, PASSWORD_VALIDATORS, USERNAME_VALIDATORS


class JoinForm(FlaskForm):
    """Form for joining."""

    username = StringField(
        label='Username',
        validators=USERNAME_VALIDATORS
    )

    password = PasswordField(
        label='Password',
        validators=PASSWORD_VALIDATORS
    )

    email = StringField(
        label='Email',
        validators=EMAIL_VALIDATORS
    )

    confirm_password = PasswordField(
        label='Confirm Password',
        validators=[EqualTo('password', message='Passwords don\'t match.')]
    )

    confirm_email = StringField(
        label='Confirm Email',
        validators=[EqualTo('email', message='Emails don\'t match.')]
    )
