from flask_wtf import FlaskForm
from wtforms import PasswordField, SubmitField
from wtforms.validators import EqualTo, InputRequired

from . import PASSWORD_VALIDATORS

class UpdatePasswordForm(FlaskForm):
    """Used to update user's password."""

    current_password = PasswordField(
        label='Current Password',
        validators=PASSWORD_VALIDATORS
    )

    new_password = PasswordField(
        label='New Password',
        validators=PASSWORD_VALIDATORS
    )

    confirm_password = PasswordField(
        label='Confirm Password',
        validators=[
            InputRequired(),
            EqualTo('new_password', message='Passwords don\'t match.')
        ]
    )

    submit = SubmitField('Update Password')
