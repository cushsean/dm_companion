from wtforms.validators import Email, InputRequired, Length

PASSWORD_VALIDATORS=[
            InputRequired('Password is required.'),
            Length(min=8, max=255, message='Password must be 8 to 255 characters.')
        ]

USERNAME_VALIDATORS=[
            InputRequired('Username is required.'),
            Length(min=2, max=32, message='Username must be 5 to 32 characters.')
        ]

EMAIL_VALIDATORS=[
            Email('Email is required.'),
            InputRequired('Email is required.')
        ]
