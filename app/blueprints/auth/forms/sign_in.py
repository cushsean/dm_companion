from flask_wtf import FlaskForm
from wtforms import PasswordField, StringField
from wtforms.validators import InputRequired

class SignInForm(FlaskForm):
    """Form for signing in."""

    username = StringField(
        label='Username',
        validators=[InputRequired('Username is required.')]
    )

    password = PasswordField(
        label='Password',
        validators=[InputRequired('Password is required.')]
    )
