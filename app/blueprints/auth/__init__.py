from flask import Blueprint, current_app
from flask_login import current_user, login_user

from .models import *

from app import login_manager

bp = Blueprint('auth', __name__, url_prefix='/auth', template_folder='templates')

def login_attempt(
    username=None,
    password=None,
    error='Username or Password Invalid.'
):
    """
    login_attempt() wraps Flask's login_user() and provides additional checks on
    the user before logging in.

    The additional checks include:
        - Checking that the user exists in the database.
        - Checking that the user is not disabled.

    Return status. If the user was logged in, None is returned. Otherwise, an
    error message is returned to be handeled by the calling function.
    """

    user = User.query.filter_by(username=username).first()

    # Verify user exists
    try:
        user = User.query.filter_by(username=username).first()
        if user.disabled == 1:
            error='This account is disabled. Please contact an administrator.'
            return error
    except:
        return error

    # Valdate password
    if user.validate_password(password):
        login_user(user)
        return None
    else:
        return error

def check_permissions(page):
    """
    This function cross references the Read/Write permissions for the
    target page, and determines if the user is permitted to Read and/or Write
    to the page.

    If the cross-reference is not successful, None is returned.
    On success, (Boolean, Boolean) is returned corrisponding to the
    (Read, Write) permissions of the user for the target page.
    """

    if page is None:
        return None

    if current_user.is_authenticated:

        levels = PagePermissions.query.filter_by(page=page).first()

        if levels is None:
            return None

        userLevel = current_user.privilege.level

        return (userLevel<=levels.read_level, userLevel<=levels.write_level)

    return (False, False)

def is_admin():
    """Check if user is Admin.

    True if the user is an Admin.

    Also contains a check for LOGIN_DISABLED for testing purposes.
    """

    if 'LOGIN_DISABLED' in current_app.config:
        if current_app.config['LOGIN_DISABLED']:
            return True
    if current_user.is_authenticated:
        if current_user.privilege.level == 1:
            return True
    return False

def is_dm():
    """Check if user is DM.

    True if the user is DM or Admin.

    Also contains a check for LOGIN_DISABLED for testing purposes.
    """

    if 'LOGIN_DISABLED' in current_app.config:
        if current_app.config['LOGIN_DISABLED']:
            return True
    if current_user.is_authenticated:
        if current_user.privilege.level <= 5 or is_admin():
            return True
    return False

# Configure Login Manager
login_manager.login_view = "auth.sign_in"

@login_manager.user_loader
def load_user(userid):
    """
    Used by flask_login internally to load users from the database.
    """
    return User.query.filter(User.id==userid).first()


from .views.change_password import change_password
from .views.join import join
from .views.sign_in import sign_in
from .views.sign_out import sign_out

bp.add_url_rule('/change_password', view_func=change_password, methods=["GET", "POST"])
bp.add_url_rule('/join', view_func=join, methods=["GET", "POST"])
bp.add_url_rule('/sign_in', view_func=sign_in, methods=["GET", "POST"])
bp.add_url_rule('/sign_out', view_func=sign_out, methods=["GET", "POST"])
