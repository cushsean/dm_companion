from flask import Blueprint, render_template, redirect, url_for, request
from flask_login import current_user, login_required

from app import db
from app.blueprints.characters.forms import ListForm
from app.blueprints.characters.models import Character

from .views.character import character

bp = Blueprint('characters', __name__, url_prefix='/characters', template_folder='templates')

@bp.route('/', methods=["GET", "POST"])
@login_required
def index():
    """
    Index for characters.

    Access:
        TBD

    Actions:
        Before character selection:
        - TODO: Users can select from character that have been shared with them.
        - TODO: DMs can select from characters that are a part of their own
                campaigns.
            - TODO: Character sheets should also be viewable in a user's profile
                    to Admins
            - TODO: Viewing all characters sheets should be toggle-able.

        After Character selection:
        - TODO: User can upload a picture of the character.
        - TODO: Upload a 3d character model.
        - TODO: Select campaign for the character.
            - TODO: Select based on DM.
            - TODO: Drop-down.
        - TODO: Add Talespire roll links and buttons.
    """

    # Make character list

    form = ListForm()

    # if request.method == "GET":
    characters = None
    if int(current_user.privilege.level) <= 5:
        characters = Character.query.all()
    else:
        characters = Character.query.filter_by(player_id=current_user.id).all()

    for _char in characters:
        data = dict(
            char_id = _char.id
        )
        form.char.append_entry(data)
        form.char.entries[-1].char_name.label.text = _char.name

    if form.validate_on_submit():

        if form.new_character.data:
            return redirect(url_for('characters.character', character_id="New Character"))

        for _char in form.char.entries:
            if _char.char_name.data:
                return redirect(url_for('characters.character', character_id=_char.char_id.data))
            elif _char.rm_btn.data:
                rm_char = Character.query.filter_by(id=_char.char_id.data).first()
                db.session.delete(rm_char)
                try:
                    db.session.commit()
                except Exception:
                    db.session.rollback()
                return redirect(url_for('characters.character', character_id=""))

    return render_template('characters/list.html', form=form)

bp.add_url_rule('/<character_id>', view_func=character, methods=["GET", "POST"])
