from app import db


class Weapons(db.Model):
    __tablename__ = 'weapons'

    # Columns
    id           = db.Column(db.Integer, primary_key=True, autoincrement=True)
    character_id = db.Column(db.Integer, db.ForeignKey('character.id'))
    name         = db.Column('Name', db.String(32), unique=False)
    att_bonus    = db.Column('Attack Bonus', db.String(32), unique=False)
    dmg          = db.Column('Damage', db.String(32), unique=False)
    crit         = db.Column('Critial', db.String(32), unique=False)
    rng          = db.Column('Range', db.String(32), unique=False)
    dmg_type     = db.Column('Damage Type', db.String(32), unique=False)
    notes        = db.Column('Notes', db.Text(), unique=False)
    md5          = db.Column('md5sum', db.String(32))


class Armor(db.Model):
    __tablename__ = 'armor'

    # Columns
    id            = db.Column(db.Integer, primary_key=True, autoincrement=True)
    character_id  = db.Column(db.Integer, db.ForeignKey('character.id'))
    name          = db.Column('Name', db.String(32), unique=False)
    typ           = db.Column('Type', db.String(32), unique=False)
    ac            = db.Column('AC Bonus', db.String(32), unique=False)
    max_dex       = db.Column('Max DEX', db.String(32), unique=False)
    check_penalty = db.Column('Check Penalty', db.String(32), unique=False)
    spell_failure = db.Column('Spell Failure', db.String(32), unique=False)
    speed         = db.Column('Speed', db.String(32), unique=False)
    weight        = db.Column('Weight', db.String(32), unique=False)
    notes         = db.Column('Notes', db.Text())
    md5           = db.Column('md5sum', db.String(32))


class Shield(db.Model):
    __tablename__ = 'shield'

    # Columns
    id            = db.Column(db.Integer, primary_key=True, autoincrement=True)
    character_id  = db.Column(db.Integer, db.ForeignKey('character.id'))
    name          = db.Column('Name', db.String(32))
    ac            = db.Column('AC Bonus', db.String(32))
    weight        = db.Column('Weight', db.String(32))
    check_penalty = db.Column('Check Penalty', db.String(32))
    spell_failure = db.Column('Spell Failure', db.String(32))
    notes         = db.Column('Notes', db.Text())
    md5           = db.Column('md5sum', db.String(32))


class ClassNLevel(db.Model):
    __tablename_ = 'class_n_level'

    # Columns
    id           = db.Column(db.Integer, primary_key=True, autoincrement=True)
    character_id = db.Column(db.Integer, db.ForeignKey('character.id'))
    class_name   = db.Column(db.String(32), unique=False)
    class_level  = db.Column(db.Integer,  unique=False)
    md5           = db.Column('md5sum', db.String(32))


class Feat(db.Model):
    __tablename__ = 'feat'

    # Columns
    id           = db.Column(db.Integer, primary_key=True, autoincrement=True)
    character_id = db.Column(db.Integer, db.ForeignKey('character.id'))
    name         = db.Column(db.String(64), unique=False)
    md5          = db.Column('md5sum', db.String(32))


class Spell(db.Model):
    __tablename__ = 'spell'

    # Columns
    id           = db.Column(db.Integer, primary_key=True, autoincrement=True)
    character_id = db.Column(db.Integer, db.ForeignKey('character.id'))
    name         = db.Column(db.String(32), unique=False)
    mana         = db.Column(db.Integer)
    brief        = db.Column(db.Text)
    damage       = db.Column(db.String(32))
    md5           = db.Column('md5sum', db.String(32))


class Specials(db.Model):
    __tablename__ = 'specials'

    # Columns
    id           = db.Column(db.Integer, primary_key=True, autoincrement=True)
    character_id = db.Column(db.Integer, db.ForeignKey('character.id'))
    name         = db.Column(db.Text, unique=False)
    md5           = db.Column('md5sum', db.String(32))


class Languages(db.Model):
    __tablename__ = 'languages'

    # Columns
    id           = db.Column(db.Integer, primary_key=True, autoincrement=True)
    character_id = db.Column(db.Integer, db.ForeignKey('character.id'))
    name         = db.Column(db.Text, unique=False)
    md5           = db.Column('md5sum', db.String(32))


class Items(db.Model):
    __tablename__ = 'items'

    # Columns
    id           = db.Column(db.Integer, primary_key=True, autoincrement=True)
    character_id = db.Column(db.Integer, db.ForeignKey('character.id'))
    name         = db.Column(db.String(64))
    quantity     = db.Column(db.Integer)
    value        = db.Column(db.String(32))
    weight       = db.Column(db.Integer)
    notes        = db.Column(db.Text)
    md5           = db.Column('md5sum', db.String(32))


class Character(db.Model):
    __tablename__ = 'character'

    # Columns
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)

    # Info
    name             = db.Column(db.String(32))
    player_id        = db.Column(db.Integer, db.ForeignKey('user.id'))
    race             = db.Column(db.String(32))
    level_adjustment = db.Column(db.Integer)
    alignment        = db.Column(db.String(32))
    deity            = db.Column(db.String(32))
    size             = db.Column(db.String(32))
    age              = db.Column(db.Integer)
    gender           = db.Column(db.String(8))
    height           = db.Column(db.String(16))
    weight           = db.Column(db.String(8))
    experience       = db.Column(db.Integer)
    backstory        = db.Column(db.Text)
    md5_info         = db.Column(db.String(32))

    # Stats
    hp             = db.Column(db.Integer)
    hp_max         = db.Column(db.Integer)
    mana           = db.Column(db.Integer)
    mana_max       = db.Column(db.Integer)
    base_atk       = db.Column(db.Integer)
    speed          = db.Column(db.Integer)
    dmg_reduction  = db.Column(db.Integer)
    spell_resist   = db.Column(db.Integer)
    arcane_failure = db.Column(db.Integer)
    actions        = db.Column(db.Integer)
    exhaustion     = db.Column(db.Integer)
    md5_stats      = db.Column(db.String(32))

    # AC
    nat_armor  = db.Column(db.Integer)
    defl_armor = db.Column(db.Integer)
    misc_armor = db.Column(db.Integer)

    md5_ac     = db.Column(db.String(32))

    # Init
    init_misc = db.Column(db.Integer)
    md5_init  = db.Column(db.String(32))

    # Abilities
    str_temp      = db.Column(db.Integer)
    str_score     = db.Column(db.Integer)
    dex_temp      = db.Column(db.Integer)
    dex_score     = db.Column(db.Integer)
    con_temp      = db.Column(db.Integer)
    con_score     = db.Column(db.Integer)
    int_temp      = db.Column(db.Integer)
    int_score     = db.Column(db.Integer)
    wis_temp      = db.Column(db.Integer)
    wis_score     = db.Column(db.Integer)
    cha_temp      = db.Column(db.Integer)
    cha_score     = db.Column(db.Integer)
    md5_abilities = db.Column(db.String(32))

    # Backref
    class_n_level = db.relationship("ClassNLevel", backref="character")
    items         = db.relationship("Items", backref="character")
    weapon        = db.relationship("Weapons", backref="character")
    armor         = db.relationship("Armor", backref="character")
    spell         = db.relationship("Spell", backref="character")
    feat          = db.relationship("Feat", backref="character")
    specials      = db.relationship("Specials", backref="character")
    languages     = db.relationship("Languages", backref="character")

    # Money
    copper    = db.Column(db.Integer)
    silver    = db.Column(db.Integer)
    gold      = db.Column(db.Integer)
    platinum  = db.Column(db.Integer)
    md5_money = db.Column(db.String(32))

    # Skills
    athletics_class_skill       = db.Column(db.String(4))
    athletics_ranks             = db.Column(db.Integer)
    athletics_class_mod         = db.Column(db.Integer)
    athletics_misc              = db.Column(db.Integer)
    knowledge_study_class_skill = db.Column(db.String(4))
    knowledge_study_ranks       = db.Column(db.Integer)
    knowledge_study_class_mod   = db.Column(db.Integer)
    knowledge_study_misc        = db.Column(db.Integer)
    knowledge_exp_class_skill   = db.Column(db.String(4))
    knowledge_exp_ranks         = db.Column(db.Integer)
    knowledge_exp_class_mod     = db.Column(db.Integer)
    knowledge_exp_misc          = db.Column(db.Integer)
    animal_handle_class_skill   = db.Column(db.String(4))
    animal_handle_ranks         = db.Column(db.Integer)
    animal_handle_class_mod     = db.Column(db.Integer)
    animal_handle_misc          = db.Column(db.Integer)
    survival_class_skill        = db.Column(db.String(4))
    survival_ranks              = db.Column(db.Integer)
    survival_class_mod          = db.Column(db.Integer)
    survival_misc               = db.Column(db.Integer)
    arcana_class_skill          = db.Column(db.String(4))
    arcana_ranks                = db.Column(db.Integer)
    arcana_class_mod            = db.Column(db.Integer)
    arcana_misc                 = db.Column(db.Integer)
    craft_class_skill           = db.Column(db.String(4))
    craft_ranks                 = db.Column(db.Integer)
    craft_class_mod             = db.Column(db.Integer)
    craft_misc                  = db.Column(db.Integer)
    insight_class_skill         = db.Column(db.String(4))
    insight_ranks               = db.Column(db.Integer)
    insight_class_mod           = db.Column(db.Integer)
    insight_misc                = db.Column(db.Integer)
    persuasion_class_skill      = db.Column(db.String(4))
    persuasion_ranks            = db.Column(db.Integer)
    persuasion_class_mod        = db.Column(db.Integer)
    persuasion_misc             = db.Column(db.Integer)
    deception_class_skill       = db.Column(db.String(4))
    deception_ranks             = db.Column(db.Integer)
    deception_class_mod         = db.Column(db.Integer)
    deception_misc              = db.Column(db.Integer)
    intimidation_class_skill    = db.Column(db.String(4))
    intimidation_ranks          = db.Column(db.Integer)
    intimidation_class_mod      = db.Column(db.Integer)
    intimidation_misc           = db.Column(db.Integer)
    investigation_class_skill   = db.Column(db.String(4))
    investigation_ranks         = db.Column(db.Integer)
    investigation_class_mod     = db.Column(db.Integer)
    investigation_misc          = db.Column(db.Integer)
    stealth_class_skill         = db.Column(db.String(4))
    stealth_ranks               = db.Column(db.Integer)
    stealth_class_mod           = db.Column(db.Integer)
    stealth_misc                = db.Column(db.Integer)
    slight_hand_class_skill     = db.Column(db.String(4))
    slight_hand_ranks           = db.Column(db.Integer)
    slight_hand_class_mod       = db.Column(db.Integer)
    slight_hand_misc            = db.Column(db.Integer)
    med_class_skill             = db.Column(db.String(4))
    med_ranks                   = db.Column(db.Integer)
    med_class_mod               = db.Column(db.Integer)
    med_misc                    = db.Column(db.Integer)
    perception_class_skill      = db.Column(db.String(4))
    perception_ranks            = db.Column(db.Integer)
    perception_class_mod        = db.Column(db.Integer)
    perception_misc             = db.Column(db.Integer)
    performance_class_skill     = db.Column(db.String(4))
    performance_ranks           = db.Column(db.Integer)
    performance_class_mod       = db.Column(db.Integer)
    performance_misc            = db.Column(db.Integer)
    fort_class_skill            = db.Column(db.String(4))
    fort_ranks                  = db.Column(db.Integer)
    fort_class_mod              = db.Column(db.Integer)
    fort_misc                   = db.Column(db.Integer)
    ref_class_skill             = db.Column(db.String(4))
    ref_ranks                   = db.Column(db.Integer)
    ref_class_mod               = db.Column(db.Integer)
    ref_misc                    = db.Column(db.Integer)
    will_class_skill            = db.Column(db.String(4))
    will_ranks                  = db.Column(db.Integer)
    will_class_mod              = db.Column(db.Integer)
    will_misc                   = db.Column(db.Integer)
    md5_skills                  = db.Column(db.String(32))

    # Notes
    notes     = db.Column(db.Text)
    md5_notes = db.Column(db.String(32))
