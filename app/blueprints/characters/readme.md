# Character's Blueprint

This Blueprint contains all the character sheets.

## Index
The inital view displays a list of all character sheets the current user owns. If the user is a DM or ADMIN, they will have access to the entire list of all character sheets.

## Existing Character Sheet
After selecting an existing character, the character's data is loaded from the database.

## New Character Sheet
A new character sheet is genereated and loaded with only default vaules. The current user is designated as the new character's owner.

## Helper

The helper files are all associated with a single class `CharacterSheet`.
There are two types of boolean values to consider:
- _set: Prefixed with a section name and is used to designate if that section has been loaded/set or if it still needs to be.
- load_db: Determines if the character's data needs loaded from the database. Used mostly for 'POST' operations.

List of sections:
- Abilities
- Armor Class
- Character Info
- Character Stats
- Class Info
- Feats
- Initative
- Items
- Languages
- Money
- Notes
- Shields
- Skills
- Specials
- Spells
- Weapons

## Known Issues
- DMs should only be able to see characters within their current party, not all in the database.
- Values do not save to the database.
- Clicking the 'X' button does not remove the item from the character sheet.
- Unable to change the owner of the character.