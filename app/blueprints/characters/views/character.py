from flask import render_template, request, redirect, url_for

from flask_login import current_user, login_required

from .helpers import CharacterSheet as CS
from app import db
from app.blueprints.characters.models import Character
from app.blueprints.characters.forms import MainForm


@login_required
def character(character_id=None):

    # Get objects
    form = MainForm()
    char = Character.query.filter_by(id=character_id).first()
    cs = CS(char, form)

    # Update Player List
    form.char_info.player.choices = [username[0] for username in db.session.execute('SELECT username from user where 1').all()]

    # Set priviliges
    if int(current_user.privilege.level) > 5:
        form.char_info.player.description['editable'] = False
    else:
        form.char_info.player.description['editable'] = True

    # Set Character
    if not char:
        char = Character(player=current_user)
        db.session.add(char)
        db.session.flush()
        db.session.commit()
        character_id=char.id

        form.char_info.player.data = current_user.username

        cs.load_char(char=char, load_db=False)
        cs.upload()

    else:
        if request.method == "GET":
            cs.load_char()
        if not form.char_info.player.data:
            form.char_info.player.data = char.player.username

    if request.method == "POST":
        if form.validate_on_submit():
            if form.add_class.data:
                cs.add_class()
            elif form.add_weapon.data:
                cs.add_weapon()
            elif form.add_armor.data:
                cs.add_armor()
            elif form.add_shield.data:
                cs.add_shield()
            elif form.add_spell.data:
                cs.add_spell()
            elif form.add_item.data:
                cs.add_item()
            elif form.add_feat.data:
                cs.add_feat()
            elif form.add_special.data:
                cs.add_special()
            elif form.add_language.data:
                cs.add_language()
            elif form.save.data or form.update.data:
                cs.upload()
            else:
                # Check for rm buttons
                check_rm_button(form.class_info, cs.rm_class)
                check_rm_button(form.weapons, cs.rm_weapon)
                check_rm_button(form.armor, cs.rm_armor)
                check_rm_button(form.shields, cs.rm_shield)
                check_rm_button(form.spells, cs.rm_spell)
                check_rm_button(form.items, cs.rm_item)
                check_rm_button(form.feats, cs.rm_feat)
                check_rm_button(form.specials, cs.rm_special)
                check_rm_button(form.languages, cs.rm_language)

            cs.load_char(load_db=False)

        else:
            print("post failed")
            for err in form.errors:
                print(err)

    cs.create_table_feats()
    try:
        if 'New Character' in request.path:
            return redirect(url_for('characters.character', character_id=character_id))
    except Exception as err:
        print(err)
        pass
    return render_template('characters/sheet.html', character_id=character_id, form=form, data=cs.data, tables=cs.tables)

def check_rm_button(form, rm):
    """ Check if the forms delete button has been clicked and removes line item. """

    for i, _form in enumerate(form.entries):
        if _form.bn_del.data:
            rm(i)