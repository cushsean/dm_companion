import hashlib

from app.blueprints.characters.forms import ArmorForm
from app.blueprints.characters.models import Armor
from app import db


def get_armor(self):
    """ Sets the character's armor based on the database. """

    _form = self.form.armor

    if not self.armor_set:

        armor = []
        if self.load_db:
            armor = Armor.query.filter_by(character_id=self.char.id).all()

        ### Independent ###
        for _armor in armor:

            data = dict(
                name_    = _armor.name,
                typ      = _armor.typ,
                ac       = _armor.ac,
                dex      = _armor.max_dex,
                check    = _armor.check_penalty,
                spell    = _armor.spell_failure,
                speed    = _armor.speed,
                weight   = _armor.weight,
                notes    = _armor.notes,
                model_id = _armor.id
            )
            data = {key: value for key, value in data.items() if value is not None}

            _form.append_entry(data)

        ### Dependent ###

        ### Calculated ###

        self.armor_set = True  # Not used, left for consistancy


def get_armor_ac(self):
    """ Return the culative ac of the character's worn armor. """

    if not self.armor_set:
        self.get_armor()
    armor_ac = 0
    for entrie in self.form.armor.entries:
        try:
            armor_ac += entrie.ac.data
        except TypeError as err:
            if entrie.ac.data is None:
                pass
            else:
                print("ERROR: app/blueprints/characters/views/helpers/armor.py:45 --", err)

    return armor_ac


def get_max_dex(self):
    """ Returns the lowest max dex allowed by all armor worn. """

    if not self.armor_set:
        self.get_armor()
    max_dex = None
    for _armor in self.form.armor.entries:
        try:
            if max_dex > _armor.dex.data:
                max_dex = _armor.dex.data
        except TypeError:
            max_dex = _armor.dex.data

    return max_dex


def get_max_speed(self):
    """ Returns the lowest speed allowed by all armor worn. """

    if not self.armor_set:
        self.get_armor()
    max_speed = None
    for _armor in self.form.armor.entries:
        try:
            if max_speed > _armor.speed.data:
                max_speed = _armor.speed.data
        except TypeError:
            max_speed = _armor.speed.data

    return max_speed


def add_armor(self):
    """ Adds a line to the armor section. """

    self.form.armor.append_entry()


def rm_armor(self, index):
    """ Removes the selected record. """

    self.rm_record(self.form.armor, Armor, index)


def upload_armor(self):
    """ Uploads data to the database. """

    _form = self.form.armor
    _char = self.char

    db_armor = Armor.query.filter_by(character_id=self.char.id).all()

    for i, armor in enumerate(_form.entries):

        if not armor.name_.data:
            continue

        checksum = self.checksum(armor)

        if i >= len(db_armor):
            db_armor.append(Armor(character_id=_char.id))
            db.session.add(db_armor[i])
            db.session.flush()
        elif db_armor[i].md5 == checksum:
            continue

        # Write Changes
        db_armor[i].name          = armor.name_.data
        db_armor[i].typ           = armor.typ.data
        db_armor[i].ac            = armor.ac.data
        db_armor[i].max_dex       = armor.dex.data
        db_armor[i].check_penalty = armor.check.data
        db_armor[i].spell_failure = armor.spell.data
        db_armor[i].speed         = armor.speed.data
        db_armor[i].weight        = armor.weight.data
        db_armor[i].notes         = armor.notes.data
        armor.model_id.data       = db_armor[i].id

        db_armor[i].md5 = checksum

        try:
            db.session.commit()
        except: # pragma: no cover
            db.session.rollback()
