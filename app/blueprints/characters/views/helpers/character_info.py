from app import db


def get_char_info(self):
    """ Sets the data for the character info fields based on data from the database. """

    _form = self.form.char_info

    if not self.char_info_set:

        ### Independent ###

        if self.load_db:
            _form.char_name.data    = self.char.name or _form.char_name.data
            _form.player.data       = self.char.player.username or _form.player.data
            _form.lvl_adj.data      = self.char.level_adjustment or _form.lvl_adj.data
            _form.race.data         = self.char.race or _form.race.data
            _form.alignment.data    = self.char.alignment or _form.alignment.data
            _form.deity.data        = self.char.deity or _form.deity.data
            _form.size.data         = self.char.size or _form.size.data
            _form.age.data          = self.char.age or _form.age.data
            _form.gender.data       = self.char.gender or _form.gender.data
            _form.height.data       = self.char.height or _form.height.data
            _form.weight.data       = self.char.weight or _form.weight.data
            _form.exp.data          = self.char.experience or _form.exp.data
            _form.back_story.data   = self.char.backstory or _form.back_story.data

        ### Dependent ###

        class_lvls = self.get_class_lvls()

        ### Calculated ###

        try:
            _form.ecl.data = class_lvls + _form.lvl_adj.data
        except Exception as err:
            print("ERROR: app/blueprints/characters/views/helpers/character_info.py:31 --", err)
            _form.ecl.data = -1

        try:
            exp_needed = int((((_form.ecl.data ** 2) + _form.ecl.data) / 2) * 1000)
            if exp_needed >= _form.exp.data:
                _form.level_up = True
            _form.next_lvl.data = str(_form.exp.data) + '/' + str(exp_needed)
        except Exception as err:
            print("ERROR: app/blueprints/characters/views/helpers/character_info.py:37-40 --", err)
            _form.next_lvl.data = "ERROR"

        self.char_info_set = True


def get_size_mod(self):
    """ Returns the size modifier for the character based on Table 8-1 of the Player's Handbook. """

    _form = self.form.char_info

    if not self.char_info_set:
        self.get_char_info()

    size_mods = dict(
        Colossal=-8,
        Gargantuan=-4,
        Huge=-2,
        Large=-1,
        Medium=0,
        Small=1,
        Tiny=2,
        Diminutive=4,
        Fine=8
    )

    return size_mods[_form.size.data]


def get_size_actual(self):
    """ Returns a string of the characters size. """
    _form = self.form.char_info

    if not self.char_info_set:
        self.get_char_info()

    return _form.size.data


def upload_char_info(self):
    """ Uploads data to the database. """

    _form = self.form.char_info
    _char = self.char

    checksum = self.checksum(_form)

    if _char.md5_info != checksum:

        _char.name             = _form.char_name.data
        _char.player_id        = db.session.execute('SELECT id FROM user WHERE username="{}"'.format(_form.player.data)).first()[0]
        _char.race             = _form.race.data
        _char.level_adjustment = _form.lvl_adj.data
        _char.alignment        = _form.alignment.data
        _char.deity            = _form.deity.data
        _char.size             = _form.size.data
        _char.age              = _form.age.data
        _char.gender           = _form.gender.data
        _char.height           = _form.height.data
        _char.weight           = _form.weight.data
        _char.experience       = _form.exp.data
        _char.backstory        = _form.back_story.data

        _char.md5_info = checksum

        try:
            db.session.commit()
        except: # pragma: no cover
            db.session.rollback()
