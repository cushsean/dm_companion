from app import db
from app.blueprints.characters.forms import SpecialsForm
from app.blueprints.characters.models import Specials


def get_specials(self):
    """ Get specials data from database. """

    _form = self.form.specials

    if not self.specials_set:

        specials = []
        if self.load_db:
            specials = Specials.query.filter_by(character_id=self.char.id).all()

        ### Independent ###

        for spec in specials:

            data = dict(
                special_ability = spec.name,
                model_id = spec.id
            )
            data = {key: value for key, value in data.items() if value is not None}

            _form.append_entry(data)

        ### Dependent ###

        ### Calculated ###

        self.specials_set = True  # Not used, left for consistancy


def add_special(self):
    """ Adds a line to the specials section. """

    self.form.specials.append_entry()


def rm_special(self, index):
    """ Removes the selected record. """

    self.rm_record(self.form.specials, Specials, index)


def upload_specials(self):
    """ Uploads data to the database. """

    _form = self.form.specials
    _char = self.char

    specs = Specials.query.filter_by(character_id=_char.id).all()

    for i, spc in enumerate(_form.entries):

        if not spc.special_ability.data:
            continue

        checksum = self.checksum(spc)

        if i >= len(specs):
            specs.append(Specials(character_id=_char.id))
            db.session.add(specs[i])
            db.session.flush()
        elif specs[i].md5 == checksum:
            continue

        # Write Changes
        specs[i].name = spc.special_ability.data
        spc.model_id.data = specs[i].id

        specs[i].md5 = checksum

    try:
        db.session.commit()
    except: # pragma: no cover
        db.session.rollback()
