from app import db
from app.blueprints.characters.forms import WeaponForm
from app.blueprints.characters.models import Weapons


def get_weapons(self):
    """ Get weapons from the database. """

    _form = self.form.weapons

    if not self.weapons_set:

        weapons = []
        if self.load_db:
            weapons = Weapons.query.filter_by(character_id=self.char.id).all()

        ### Independent ###

        for weapon in weapons:

            data = dict(
                name_ = weapon.name,
                atk_bonus = weapon.att_bonus,
                dmg = weapon.dmg,
                crit = weapon.crit,
                rng = weapon.rng,
                dmg_type = weapon.dmg_type,
                notes = weapon.notes,
                model_id = weapon.id
            )
            data = {key: value for key, value in data.items() if value is not None}

            _form.append_entry(data)

        ### Dependent ###

        ### Calculated ###

        self.weapons_set = True  # Not used, left for consistancy


def add_weapon(self):
    """ Adds a line to the weapons section. """

    self.form.weapons.append_entry()


def rm_weapon(self, index):
    """ Removes the selected record. """

    self.rm_record(self.form.weapons, Weapons, index)


def upload_weapons(self):
    """ Uploads data to the database. """

    _form = self.form.weapons
    _char = self.char

    db_weapons = Weapons.query.filter_by(character_id=_char.id).all()

    for i, _weapon in enumerate(_form.entries):

        if not _weapon.name_.data:
            continue

        checksum = self.checksum(_weapon)

        if i >= len(db_weapons):
            db_weapons.append(Weapons(character_id=_char.id))
            db.session.add(db_weapons[i])
            db.session.flush()
        elif db_weapons[i].md5 == checksum:
            continue

        # Write Changes
        db_weapons[i].name      = _weapon.name_.data
        db_weapons[i].att_bonus = _weapon.atk_bonus.data
        db_weapons[i].dmg       = _weapon.dmg.data
        db_weapons[i].crit      = _weapon.crit.data
        db_weapons[i].rng       = _weapon.rng.data
        db_weapons[i].dmg_type  = _weapon.dmg_type.data
        db_weapons[i].notes     = _weapon.notes.data
        _weapon.model_id.data   = db_weapons[i].id

        db_weapons[i].md5 = checksum

        try:
            db.session.commit()
        except: # pragma: no cover
            db.session.rollback()
