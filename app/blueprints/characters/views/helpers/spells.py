from app import db
from app.blueprints.characters.forms import SpellForm
from app.blueprints.characters.models import Spell


def get_spells(self):
    """ Get spells form database. """

    _form = self.form.spells

    if not self.spells_set:

        spells = []
        if self.load_db:
            spells = Spell.query.filter_by(character_id=self.char.id).all()

        ### Independent ###

        for spell in spells:

            data = dict(
                name_ = spell.name,
                mana = spell.mana,
                brief = spell.brief,
                model_id = spell.id
            )

            _form.append_entry(data)

        ### Dependent ###

        ### Calculated ###

        self.spells_set = True  # Not used, left for consistancy


def add_spell(self):
    """ Adds a line to the spells section. """

    self.form.spells.append_entry()


def rm_spell(self, index):
    """ Removes the selected record. """

    self.rm_record(self.form.spells, Spell, index)


def upload_spells(self):
    """ Uploads data to the database. """

    _form = self.form.spells
    _char = self.char

    db_spells = Spell.query.filter_by(character_id=_char.id).all()

    for i, _spell in enumerate(_form.entries):

        if not _spell.name_.data:
            continue

        checksum = self.checksum(_spell)

        if i >= len(db_spells):
            db_spells.append(Spell(character_id=_char.id))
            db.session.add(db_spells[i])
            db.session.flush()
        elif db_spells[i].md5 == checksum:
            continue

        # Write Changes
        db_spells[i].name  = _spell.name_.data
        db_spells[i].mana  = _spell.mana.data
        db_spells[i].brief = _spell.brief.data
        _spell.model_id.data = db_spells[i].id

        db_spells[i].md5 = checksum

        try:
            db.session.commit()
        except: # pragma: no cover
            db.session.rollback()
