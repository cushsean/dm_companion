from app import db


def get_money(self):
    """ Get money from db. """

    _form = self.form.money

    if not self.money_set:

        ### Independent ###

        if self.load_db:
            _form.cp.data = self.char.copper or _form.cp.data
            _form.sp.data = self.char.silver or _form.sp.data
            _form.gp.data = self.char.gold or _form.gp.data
            _form.pp.data = self.char.platinum or _form.pp.data

        ### Dependent ###

        ### Calculated ###

        self.money_set = True


def upload_money(self):
    """ Uploads data to the database. """

    _form = self.form.money
    _char = self.char

    checksum = self.checksum(_form)

    if _char.md5_money != checksum:

        _char.copper   = _form.cp.data
        _char.silver   = _form.sp.data
        _char.gold     = _form.gp.data
        _char.platinum = _form.pp.data

        _char.md5_money = checksum

        try:
            db.session.commit()
        except: # pragma: no cover
            db.session.rollback()
