from app import db
from app.blueprints.characters.forms import ItemsForm
from app.blueprints.characters.models import Items


def get_items(self):
    """ Get the character's items from db. """

    _form = self.form.items

    if not self.items_set:

        items = []
        if self.load_db:
            items = Items.query.filter_by(character_id=self.char.id).all()

        ### Independent ###

        for item in items:

            data = dict(
                name_    = item.name,
                quantity = item.quantity,
                value    = item.value,
                weight   = item.weight,
                notes    = item.notes,
                model_id = item.id
            )
            data = {key: value for key, value in data.items() if value is not None}

            _form.append_entry(data)

        ### Dependent ###

        ### Calculated ###

        self.items_set = True  # Not used, left for consistancy


def add_item(self):
    """ Adds a line to the item section. """

    self.form.items.append_entry()


def rm_item(self, index):
    """ Removes the selected record. """

    self.rm_record(self.form.items, Items, index)


def upload_items(self):
    """ Uploads data to the database. """

    _form = self.form.items
    _char = self.char

    db_items = Items.query.filter_by(character_id=_char.id).all()

    for i, _item in enumerate(_form.entries):

        if not _item.name_.data:
            continue

        checksum = self.checksum(_item)

        if i >= len(db_items):
            db_items.append(Items(character_id=_char.id))
            db.session.add(db_items[i])
            db.session.flush()
        elif db_items[i].md5 == checksum:
            continue

        # Write Changes
        db_items[i].name = _item.name_.data
        db_items[i].quantity = _item.quantity.data
        db_items[i].value = _item.value.data
        db_items[i].weight = _item.weight.data
        db_items[i].notes = _item.notes.data
        _item.model_id.data = db_items[i].id

        db_items[i].md5 = checksum

        try:
            db.session.commit()
        except: # pragma: no cover
            db.session.rollback()
