from app import db


def get_ac(self):
    """ Retrieve ac values from database. """

    _form = self.form.ac

    if not self.ac_set:

        ### Independent ###

        _form.base.data = 10

        if self.load_db:
            _form.natural.data    = self.char.nat_armor or _form.natural.data
            _form.deflection.data = self.char.defl_armor or _form.deflection.data
            _form.misc.data       = self.char.misc_armor or _form.misc.data

        ### Dependent ###
        _form.armor.data  = self.get_armor_ac()
        _form.shield.data = self.get_shield_ac()
        _form.dex.data    = self.get_dex_mod()
        _form.size.data   = self.get_size_mod()

        ### Calculated ###
        _form.total.data = (_form.base.data
                            + _form.armor.data
                            + _form.shield.data
                            + _form.dex.data
                            + _form.size.data
                            + _form.natural.data
                            + _form.shield.data
                            + _form.deflection.data
                            + _form.misc.data)

        self.ac_set = True


def upload_ac(self):
    """ Uploads data to the database. """

    _form = self.form.ac
    _char = self.char

    checksum = self.checksum(_form)

    if _char.md5_ac != checksum:

        _char.nat_armor  = _form.natural.data
        _char.defl_armor = _form.deflection.data
        _char.misc_armor = _form.misc.data

        _char.md5_ac = checksum

    try:
        db.session.commit()
    except: # pragma: no cover
        db.session.rollback()
