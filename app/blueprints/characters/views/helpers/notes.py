from app import db


def get_notes(self):

    _form = self.form.notes

    if not self.notes_set:

        ### Independent ###

        if self.load_db:
            _form.notes.data = self.char.notes or _form.notes.data

        ### Dependent ###

        ### Calculated ###

        self.notes_set = True


def upload_notes(self):
    """ Uploads data to the database. """

    checksum = self.checksum(self.form.notes)

    if self.char.md5_notes != checksum:

        self.char.notes = self.form.notes.notes.data

        self.char.md5_notes = checksum

        try:
            db.session.commit()
        except: # pragma: no cover
            db.session.rollback()
