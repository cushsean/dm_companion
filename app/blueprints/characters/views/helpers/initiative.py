from app import db


def get_init(self):
    """ Sets the data for the initiative fields based on data from the database. """

    _form = self.form.init

    if not self.initiative_set:

        ### Independent ###

        if self.load_db:
            _form.misc.data = self.char.init_misc or _form.misc.data

        ### Dependent ###

        _form.dex.data = self.get_dex_mod()

        ### Calculated ###

        try:
            _form.total.data = _form.dex.data + _form.misc.data
        except TypeError as err:
            print("ERROR: /app/blueprints/characters/views/helpers/initiative.py: 20 --", err)
            _form.total.data = -1

        sign_flag =  ''
        if _form.total.data >= 0:
            sign_flag = '+'
        _form.talespire += sign_flag + str(_form.total.data)

        self.initiative_set = True


def upload_init(self):
    """ Uploads data to the database. """

    checksum = self.checksum(self.form.init)

    if self.char.md5_init != checksum:

        self.char.init_misc = self.form.init.misc.data

        self.char.md5_init = checksum

        try:
            db.session.commit()
        except: # pragma: no cover
            db.session.rollback()
