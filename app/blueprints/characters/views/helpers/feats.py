from app import db
from app.blueprints.characters.forms import FeatForm
from app.blueprints.characters.models import Feat


def get_feats(self):
    """ Sets feats fields based on data from database. """

    _form = self.form.feats

    if not self.feats_set:

        char_feats = []
        if self.load_db:
            char_feats = Feat.query.filter_by(character_id=self.char.id).all()

        ### Independent ###

        for feat in char_feats:

            data = dict(
                feat = feat.name,
                model_id = feat.id
            )
            data = {key: value for key, value in data.items() if value is not None}

            _form.append_entry(data)

        ### Dependent ###

        ### Calculated ###

        self.feats_set = True  # Not used, left for consistancy


def add_feat(self):
    """ Adds a line to the feat section. """

    self.form.feats.append_entry()


def rm_feat(self, index):
    """ Removes the selected record. """

    self.rm_record(self.form.feats, Feat, index)


def upload_feats(self):
    """ Uploads data to the database. """

    _form = self.form.feats
    _char = self.char

    feats = Feat.query.filter_by(character_id=_char.id).all()

    for i, _feat in enumerate(_form.entries):

        if not _feat.feat.data:
            continue

        checksum = self.checksum(_feat)

        if i >= len(feats):
            feats.append(Feat(character_id=_char.id))
            db.session.add(feats[i])
            db.session.flush()
        elif feats[i].md5 == checksum:
            continue

        # Write Changes
        feats[i].name = _feat.feat.data
        _feat.model_id.data = feats[i].id

        feats[i].md5 = checksum

        try:
            db.session.commit()
        except: # pragma: no cover
            db.session.rollback()


# Table
from flask_table import Table, Col

class FeatTable(Table):
    feat = Col('Feat')
    bn_del = Col('')
    model_id = Col('')

def create_table_feats(self):
    """ Creates a table of data to be used in the template. """

    items = []
    for _feat in self.form.feats.entries:
        items.append(_feat)

    self.tables['feats'] = FeatTable(items)
