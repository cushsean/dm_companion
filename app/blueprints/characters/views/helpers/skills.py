from app import db


def get_skills(self):
    """ Get the skills data from the database. """

    _form = self.form.skills

    if not self.skills_set:

        if self.load_db:

            _char = self.char

            athletics       = _form.entries[0]
            knowledge_study = _form.entries[1]
            knowledge_exp   = _form.entries[2]
            animal_handle   = _form.entries[3]
            survival        = _form.entries[4]
            arcana          = _form.entries[5]
            craft           = _form.entries[6]
            insight         = _form.entries[7]
            persuasion      = _form.entries[8]
            deception       = _form.entries[9]
            intimidation    = _form.entries[10]
            investigation   = _form.entries[11]
            stealth         = _form.entries[12]
            slight_hand     = _form.entries[13]
            med             = _form.entries[14]
            perception      = _form.entries[15]
            performance     = _form.entries[16]
            fort            = _form.entries[17]
            ref             = _form.entries[18]
            will            = _form.entries[19]

            ### Independent ###

            if _char.athletics_class_skill == "1":
                athletics.class_skill.data = True
            else:
                athletics.class_skill.data = False
            athletics.ranks.data      = _char.athletics_ranks or athletics.ranks.data
            athletics.class_mod.data  = _char.athletics_class_mod or athletics.class_mod.data
            athletics.misc_mod.data   = _char.athletics_misc or athletics.misc_mod.data

            if _char.knowledge_study_class_skill == "1":
                knowledge_study.class_skill.data = True
            else:
                knowledge_study.class_skill.data = False
            knowledge_study.ranks.data      = _char.knowledge_study_ranks or knowledge_study.ranks.data
            knowledge_study.class_mod.data  = _char.knowledge_study_class_mod or knowledge_study.class_mod.data
            knowledge_study.misc_mod.data   = _char.knowledge_study_misc or knowledge_study.misc_mod.data

            if _char.knowledge_exp_class_skill == "1":
                knowledge_exp.class_skill.data = True
            else:
                knowledge_exp.class_skill.data = False
            knowledge_exp.ranks.data      = _char.knowledge_exp_ranks or knowledge_exp.ranks.data
            knowledge_exp.class_mod.data  = _char.knowledge_exp_class_mod or knowledge_exp.class_mod.data
            knowledge_exp.misc_mod.data   = _char.knowledge_exp_misc or knowledge_exp.misc_mod.data

            if _char.animal_handle_class_skill == "1":
                animal_handle.class_skill.data = True
            else:
                animal_handle.class_skill.data = False
            animal_handle.ranks.data      = _char.animal_handle_ranks or animal_handle.ranks.data
            animal_handle.class_mod.data  = _char.animal_handle_class_mod or animal_handle.class_mod.data
            animal_handle.misc_mod.data   = _char.animal_handle_misc or animal_handle.misc_mod.data

            if _char.survival_class_skill == "1":
                survival.class_skill.data = True
            else:
                survival.class_skill.data = False
            survival.ranks.data      = _char.survival_ranks or survival.ranks.data
            survival.class_mod.data  = _char.survival_class_mod or survival.class_mod.data
            survival.misc_mod.data   = _char.survival_misc or survival.misc_mod.data

            if _char.arcana_class_skill == "1":
                arcana.class_skill.data = True
            else:
                arcana.class_skill.data = False
            arcana.ranks.data      = _char.arcana_ranks or arcana.ranks.data
            arcana.class_mod.data  = _char.arcana_class_mod or arcana.class_mod.data
            arcana.misc_mod.data   = _char.arcana_misc or arcana.misc_mod.data

            if _char.craft_class_skill == "1":
                craft.class_skill.data = True
            else:
                craft.class_skill.data = False
            craft.ranks.data      = _char.craft_ranks or craft.ranks.data
            craft.class_mod.data  = _char.craft_class_mod or craft.class_mod.data
            craft.misc_mod.data   = _char.craft_misc or craft.misc_mod.data

            if _char.insight_class_skill == "1":
                insight.class_skill.data = True
            else:
                insight.class_skill.data = False
            insight.ranks.data      = _char.insight_ranks or insight.ranks.data
            insight.class_mod.data  = _char.insight_class_mod or insight.class_mod.data
            insight.misc_mod.data   = _char.insight_misc or insight.misc_mod.data

            if _char.persuasion_class_skill == "1":
                persuasion.class_skill.data = True
            else:
                persuasion.class_skill.data = False
            persuasion.ranks.data      = _char.persuasion_ranks or persuasion.ranks.data
            persuasion.class_mod.data  = _char.persuasion_class_mod or persuasion.class_mod.data
            persuasion.misc_mod.data   = _char.persuasion_misc or persuasion.misc_mod.data

            if _char.deception_class_skill == "1":
                deception.class_skill.data = True
            else:
                deception.class_skill.data = False
            deception.ranks.data      = _char.deception_ranks or deception.ranks.data
            deception.class_mod.data  = _char.deception_class_mod or deception.class_mod.data
            deception.misc_mod.data   = _char.deception_misc or deception.misc_mod.data

            if _char.intimidation_class_skill == "1":
                intimidation.class_skill.data = True
            else:
                intimidation.class_skill.data = False
            intimidation.ranks.data      = _char.intimidation_ranks or intimidation.ranks.data
            intimidation.class_mod.data  = _char.intimidation_class_mod or intimidation.class_mod.data
            intimidation.misc_mod.data   = _char.intimidation_misc or intimidation.misc_mod.data

            if _char.investigation_class_skill == "1":
                investigation.class_skill.data = True
            else:
                investigation.class_skill.data = False
            investigation.ranks.data      = _char.investigation_ranks or investigation.ranks.data
            investigation.class_mod.data  = _char.investigation_class_mod or investigation.class_mod.data
            investigation.misc_mod.data   = _char.investigation_misc or investigation.misc_mod.data

            if _char.stealth_class_skill == "1":
                stealth.class_skill.data = True
            else:
                stealth.class_skill.data = False
            stealth.ranks.data      = _char.stealth_ranks or stealth.ranks.data
            stealth.class_mod.data  = _char.stealth_class_mod or stealth.class_mod.data
            stealth.misc_mod.data   = _char.stealth_misc or stealth.misc_mod.data

            if _char.slight_hand_class_skill == "1":
                slight_hand.class_skill.data = True
            else:
                slight_hand.class_skill.data = False
            slight_hand.ranks.data      = _char.slight_hand_ranks or slight_hand.ranks.data
            slight_hand.class_mod.data  = _char.slight_hand_class_mod or slight_hand.class_mod.data
            slight_hand.misc_mod.data   = _char.slight_hand_misc or slight_hand.misc_mod.data

            if _char.med_class_skill == "1":
                med.class_skill.data = True
            else:
                med.class_skill.data = False
            med.ranks.data      = _char.med_ranks or med.ranks.data
            med.class_mod.data  = _char.med_class_mod or med.class_mod.data
            med.misc_mod.data   = _char.med_misc or med.misc_mod.data

            if _char.perception_class_skill == "1":
                perception.class_skill.data = True
            else:
                perception.class_skill.data = False
            perception.ranks.data      = _char.perception_ranks or perception.ranks.data
            perception.class_mod.data  = _char.perception_class_mod or perception.class_mod.data
            perception.misc_mod.data   = _char.perception_misc or perception.misc_mod.data

            if _char.performance_class_skill == "1":
                performance.class_skill.data = True
            else:
                performance.class_skill.data = False
            performance.ranks.data      = _char.performance_ranks or performance.ranks.data
            performance.class_mod.data  = _char.performance_class_mod or performance.class_mod.data
            performance.misc_mod.data   = _char.performance_misc or performance.misc_mod.data

            if _char.fort_class_skill == "1":
                fort.class_skill.data = True
            else:
                fort.class_skill.data = False
            fort.ranks.data      = _char.fort_ranks or fort.ranks.data
            fort.class_mod.data  = _char.fort_class_mod or fort.class_mod.data
            fort.misc_mod.data   = _char.fort_misc or fort.misc_mod.data

            if _char.ref_class_skill == "1":
                ref.class_skill.data = True
            else:
                ref.class_skill.data = False
            ref.ranks.data      = _char.ref_ranks or ref.ranks.data
            ref.class_mod.data  = _char.ref_class_mod or ref.class_mod.data
            ref.misc_mod.data   = _char.ref_misc or ref.misc_mod.data

            if _char.will_class_skill == "1":
                will.class_skill.data = True
            else:
                will.class_skill.data = False
            will.ranks.data      = _char.will_ranks or will.ranks.data
            will.class_mod.data  = _char.will_class_mod or will.class_mod.data
            will.misc_mod.data   = _char.will_misc or will.misc_mod.data

        ### Dependent ###

        self.prep_skills()

        ### Calculated ###

        self.skills_set = True


def prep_skills(self):
    """ Sets everything for skills that relies on abilities. """

    if not self.skills_set:
        self.get_abilities()

    str_form = self.form.abilities.entries[0]
    dex_form = self.form.abilities.entries[1]
    con_form = self.form.abilities.entries[2]
    int_form = self.form.abilities.entries[3]
    wis_form = self.form.abilities.entries[4]
    cha_form = self.form.abilities.entries[5]

    skill_meta = [
        ("Athletics",               dex_form, str_form),
        ("Knowledge (Study)",       int_form, None),
        ("Knowledge (Experience)",  wis_form, None),
        ("Animal Handeling",        wis_form, None),
        ("Survial",                 wis_form, None),
        ("Arcana",                  int_form, wis_form),
        ("Craft",                   int_form, None),
        ("Insight",                 wis_form, None),
        ("Persuasion",              cha_form, None),
        ("Deception",               cha_form, int_form),
        ("Intimidation",            cha_form, str_form),
        ("Investigation (Active)",  int_form, None),
        ("Stealth",                 dex_form, None),
        ("Slight of Hand",          dex_form, None),
        ("Medicine",                wis_form, None),
        ("Perception (Passive)",    wis_form, None),
        ("Performance",             cha_form, None),
        ("Fortitude",               con_form, None),
        ("Reflex",                  dex_form, None),
        ("Will",                    wis_form, None)
    ]

    for i, subform in enumerate(self.form.skills.entries):
        subform.name_.data   = skill_meta[i][0]
        subform.key.data     = skill_meta[i][1].name_.data
        subform.ability.data = str(skill_meta[i][1].mod.data)
        subtotal = 0
        try:
            subtotal = subform.ranks.data + subform.class_mod.data + subform.misc_mod.data
        except TypeError as err:
            print("ERROR: /app/blueprints/characters/views/helpers/skills.py: 245 --", err)

        subform.total.data   = skill_meta[i][1].mod.data + subtotal
        subform.alt_total.data = str(subform.total.data)

        sign_flag = ''
        if subform.total.data >= 0:
            sign_flag = '+'
        else:
            sign_flag = ''
        subform.talespire += sign_flag + subform.alt_total.data

        if skill_meta[i][2]:
            subform.key.data += "/" + skill_meta[i][2].name_.data
            subform.ability.data += "/" + str(skill_meta[i][2].mod.data)
            subform.alt_total.data += "/" + str(skill_meta[i][2].mod.data + subtotal)

            if (skill_meta[i][2].mod.data + subtotal) >= 0:
                sign_flag = '+'
            else:
                sign_flag = ''
            subform.talespire_alt += sign_flag + str(skill_meta[i][2].mod.data + subtotal)
            subform.talespire_alt_bool = True


def upload_skills(self):
    """ Uploads data to the database. """

    _form = self.form.skills
    _char = self.char

    athletics       = _form.entries[0]
    knowledge_study = _form.entries[1]
    knowledge_exp   = _form.entries[2]
    animal_handle   = _form.entries[3]
    survival        = _form.entries[4]
    arcana          = _form.entries[5]
    craft           = _form.entries[6]
    insight         = _form.entries[7]
    persuasion      = _form.entries[8]
    deception       = _form.entries[9]
    intimidation    = _form.entries[10]
    investigation   = _form.entries[11]
    stealth         = _form.entries[12]
    slight_hand     = _form.entries[13]
    med             = _form.entries[14]
    perception      = _form.entries[15]
    performance     = _form.entries[16]
    fort            = _form.entries[17]
    ref             = _form.entries[18]
    will            = _form.entries[19]

    checksum = self.checksum([
        athletics,
        knowledge_study,
        knowledge_exp,
        animal_handle,
        survival,
        arcana,
        craft,
        insight,
        persuasion,
        deception,
        intimidation,
        investigation,
        stealth,
        slight_hand,
        med,
        perception,
        performance,
        fort,
        ref,
        will
    ])

    if _char.md5_skills != checksum:

        _char.athletics_class_skill = athletics.class_skill.data
        _char.athletics_ranks       = athletics.ranks.data
        _char.athletics_class_mod   = athletics.class_mod.data
        _char.athletics_misc        = athletics.misc_mod.data

        _char.knowledge_study_class_skill = knowledge_study.class_skill.data
        _char.knowledge_study_ranks       = knowledge_study.ranks.data
        _char.knowledge_study_class_mod   = knowledge_study.class_mod.data
        _char.knowledge_study_misc        = knowledge_study.misc_mod.data

        _char.knowledge_exp_class_skill = knowledge_exp.class_skill.data
        _char.knowledge_exp_ranks       = knowledge_exp.ranks.data
        _char.knowledge_exp_class_mod   = knowledge_exp.class_mod.data
        _char.knowledge_exp_misc        = knowledge_exp.misc_mod.data

        _char.animal_handle_class_skill = animal_handle.class_skill.data
        _char.animal_handle_ranks       = animal_handle.ranks.data
        _char.animal_handle_class_mod   = animal_handle.class_mod.data
        _char.animal_handle_misc        = animal_handle.misc_mod.data

        _char.survival_class_skill = survival.class_skill.data
        _char.survival_ranks       = survival.ranks.data
        _char.survival_class_mod   = survival.class_mod.data
        _char.survival_misc        = survival.misc_mod.data

        _char.arcana_class_skill = arcana.class_skill.data
        _char.arcana_ranks       = arcana.ranks.data
        _char.arcana_class_mod   = arcana.class_mod.data
        _char.arcana_misc        = arcana.misc_mod.data

        _char.craft_class_skill = craft.class_skill.data
        _char.craft_ranks       = craft.ranks.data
        _char.craft_class_mod   = craft.class_mod.data
        _char.craft_misc        = craft.misc_mod.data

        _char.insight_class_skill = insight.class_skill.data
        _char.insight_ranks       = insight.ranks.data
        _char.insight_class_mod   = insight.class_mod.data
        _char.insight_misc        = insight.misc_mod.data

        _char.persuasion_class_skill = persuasion.class_skill.data
        _char.persuasion_ranks       = persuasion.ranks.data
        _char.persuasion_class_mod   = persuasion.class_mod.data
        _char.persuasion_misc        = persuasion.misc_mod.data

        _char.deception_class_skill = deception.class_skill.data
        _char.deception_ranks       = deception.ranks.data
        _char.deception_class_mod   = deception.class_mod.data
        _char.deception_misc        = deception.misc_mod.data

        _char.intimidation_class_skill = intimidation.class_skill.data
        _char.intimidation_ranks       = intimidation.ranks.data
        _char.intimidation_class_mod   = intimidation.class_mod.data
        _char.intimidation_misc        = intimidation.misc_mod.data

        _char.investigation_class_skill = investigation.class_skill.data
        _char.investigation_ranks       = investigation.ranks.data
        _char.investigation_class_mod   = investigation.class_mod.data
        _char.investigation_misc        = investigation.misc_mod.data

        _char.stealth_class_skill = stealth.class_skill.data
        _char.stealth_ranks       = stealth.ranks.data
        _char.stealth_class_mod   = stealth.class_mod.data
        _char.stealth_misc        = stealth.misc_mod.data

        _char.slight_hand_class_skill = slight_hand.class_skill.data
        _char.slight_hand_ranks       = slight_hand.ranks.data
        _char.slight_hand_class_mod   = slight_hand.class_mod.data
        _char.slight_hand_misc        = slight_hand.misc_mod.data

        _char.med_class_skill = med.class_skill.data
        _char.med_ranks       = med.ranks.data
        _char.med_class_mod   = med.class_mod.data
        _char.med_misc        = med.misc_mod.data

        _char.perception_class_skill = perception.class_skill.data
        _char.perception_ranks       = perception.ranks.data
        _char.perception_class_mod   = perception.class_mod.data
        _char.perception_misc        = perception.misc_mod.data

        _char.performance_class_skill = performance.class_skill.data
        _char.performance_ranks       = performance.ranks.data
        _char.performance_class_mod   = performance.class_mod.data
        _char.performance_misc        = performance.misc_mod.data

        _char.fort_class_skill = fort.class_skill.data
        _char.fort_ranks       = fort.ranks.data
        _char.fort_class_mod   = fort.class_mod.data
        _char.fort_misc        = fort.misc_mod.data

        _char.ref_class_skill = ref.class_skill.data
        _char.ref_ranks       = ref.ranks.data
        _char.ref_class_mod   = ref.class_mod.data
        _char.ref_misc        = ref.misc_mod.data

        _char.will_class_skill = will.class_skill.data
        _char.will_ranks       = will.ranks.data
        _char.will_class_mod   = will.class_mod.data
        _char.will_misc        = will.misc_mod.data

        _char.md5_skills = checksum

        try:
            db.session.commit()
        except: # pragma: no cover
            db.session.rollback()
