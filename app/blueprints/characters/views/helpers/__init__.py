import hashlib
from wtforms import Form

from  app import db


class CharacterSheet:
    """ A class containing all the helper functions for the character sheet. """
    def __init__(self, char, form, load_db=True, data: dict={}, tables: dict={}):
        """
        char: Holds the character database model.
        form: A copy of the active form.
        load_db: A Boolean specifying if new data needs loaded from the DB or using existing POST data.
        data: A dictionary of non-form data. The key will be used as the table's header unless prefixed by a '_', in
              which case, the header will be an empty string.
        """
        self.char = char
        self.form = form
        self.data = data
        self.tables = tables

        self.load_db = load_db

        self.abilities_set  = False
        self.ac_set         = False
        self.armor_set      = False
        self.char_info_set  = False
        self.char_stats_set = False
        self.class_info_set = False
        self.feats_set      = False
        self.initiative_set = False
        self.items_set      = False
        self.languages_set  = False
        self.money_set      = False
        self.notes_set      = False
        self.shields_set    = False
        self.skills_set     = False
        self.specials_set   = False
        self.spells_set     = False
        self.weapons_set    = False

    from .abilities import get_abilities, get_dex_mod, prep_abilities, upload_abilities, get_str_score_and_mod
    from .armor_class import get_ac, upload_ac
    from .armor import get_armor, get_armor_ac, add_armor, upload_armor, rm_armor, get_max_dex, get_max_speed
    from .character_info import get_char_info, get_size_mod, get_size_actual, upload_char_info
    from .character_stats import get_char_stats, upload_char_stats
    from .class_info import get_class_info, get_class_lvls, add_class, upload_class_info, rm_class
    from .feats import get_feats, add_feat, upload_feats, rm_feat, create_table_feats
    from .initiative import get_init, upload_init
    from .items import get_items, add_item, upload_items, rm_item
    from .languages import get_languages, add_language, upload_languages, rm_language
    from .money import get_money, upload_money
    from .notes import get_notes, upload_notes
    from .shields import get_shields, get_shield_ac, add_shield, upload_shields, rm_shield
    from .skills import get_skills, prep_skills, upload_skills
    from .specials import get_specials, add_special, upload_specials, rm_special
    from .spells import get_spells, add_spell, upload_spells, rm_spell
    from .weapons import get_weapons, add_weapon, upload_weapons, rm_weapon

    def load_char(self, char=None, load_db=True):

        if char:
            self.char = char

        self.load_db = load_db

        self.get_abilities()
        self.get_ac()
        self.get_armor()
        self.get_char_info()
        self.get_char_stats()
        self.get_class_info()
        self.get_feats()
        self.get_init()
        self.get_items()
        self.get_languages()
        self.get_money()
        self.get_notes()
        self.get_shields()
        self.get_skills()
        self.get_specials()
        self.get_spells()
        self.get_weapons()

        self.abilities_set  = False
        self.ac_set         = False
        self.armor_set      = False
        self.char_info_set  = False
        self.char_stats_set = False
        self.class_info_set = False
        self.feats_set      = False
        self.initiative_set = False
        self.items_set      = False
        self.languages_set  = False
        self.money_set      = False
        self.notes_set      = False
        self.shields_set    = False
        self.skills_set     = False
        self.specials_set   = False
        self.spells_set     = False
        self.weapons_set    = False


    def upload(self):
        """ Uploads current character sheet to the database. """

        self.upload_abilities()
        self.upload_ac()
        self.upload_armor()
        self.upload_char_info()
        self.upload_char_stats()
        self.upload_class_info()
        self.upload_feats()
        self.upload_init()
        self.upload_items()
        self.upload_languages()
        self.upload_money()
        self.upload_notes()
        self.upload_shields()
        self.upload_skills()
        self.upload_specials()
        self.upload_spells()
        self.upload_weapons()


    def checksum(self, my_forms):
        """ Iterates through a form and creates an md5sum of the contents. """

        check_str = ""

        if not isinstance(my_forms, list):
            my_forms = [my_forms]

        for _form in my_forms:
            for field in _form:
                if field.description["sum"]:
                    check_str += str(field.data)

        if check_str:
            return hashlib.md5(check_str.encode('utf-8')).hexdigest()


    def rm_record(self, form, model, index):
        """ Removes the selected record. """

        model_id = form.entries[index].model_id.data

        db_record = model.query.filter_by(id=model_id).first()
        if db_record:
            db.session.delete(db_record)
            try:
                db.session.commit()
            except:
                db.session.rollback()

        form.entries.pop(index)
        form.last_index -= 1