from app import db
from app.blueprints.characters.forms import ClassForm
from app.blueprints.characters.models import ClassNLevel


def get_class_info(self):
    """ Get all class info from db. """

    _form = self.form.class_info

    if not self.class_info_set:

        classes = []
        if self.load_db:
            classes = ClassNLevel.query.filter_by(character_id=self.char.id).all()
            if classes:
                _form.pop_entry()

        ### Independent ###

        for _class in classes:

            data = dict(
                name_ = _class.class_name,
                level = _class.class_level,
                model_id = _class.id
            )
            data = {key: value for key, value in data.items() if value is not None}

            _form.append_entry(data)

        ### Dependent ###

        ### Calculated ###

        self.class_info_set = True


def get_class_lvls(self):
    """ Return the cumulative levels of all classes. """

    _form = self.form.class_info

    if not self.class_info_set:
        self.get_class_info()

    lvls = 0
    for _class in _form.entries:
        try:
            lvls += _class.level.data
        except TypeError as err:
            if _class.level is None:
                pass
            else:
                print("ERROR: app/blueprints/characters/views/helpers/class_info.py:44 --", err)

    return lvls


def add_class(self):
    """ Adds an additional class line to the character sheet. """

    _form = self.form.class_info

    if _form.entries[0].name_.data:
        _form.append_entry()


def rm_class(self, index):
    """ Removes the selected record. """

    self.rm_record(self.form.class_info, ClassNLevel, index)


def upload_class_info(self):
    """ Uploads data to the database. """

    _form = self.form.class_info
    _char = self.char

    classes = ClassNLevel.query.filter_by(character_id=_char.id).all()

    for i, _class in enumerate(_form.entries):

        if not _class.name_.data:
            continue

        checksum = self.checksum(_class)

        if i >= len(classes):
            classes.append(ClassNLevel(character_id=_char.id))
            db.session.add(classes[i])
            db.session.flush()
        elif classes[i].md5 == checksum:
            continue

        # Write Changes
        classes[i].class_name  = _class.name_.data
        classes[i].class_level = _class.level.data
        _class.model_id.data   = classes[i].id

        classes[i].md5 = checksum

        try:
            db.session.commit()
        except: # pragma: no cover
            db.session.rollback()
