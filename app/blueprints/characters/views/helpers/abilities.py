from app import db


def get_abilities(self):
    """ Load ability values from database. """

    _form = self.form.abilities

    if not self.abilities_set:

        str_form = _form.entries[0]
        dex_form = _form.entries[1]
        con_form = _form.entries[2]
        int_form = _form.entries[3]
        wis_form = _form.entries[4]
        cha_form = _form.entries[5]

        ### Independent ###

        self.prep_abilities()

        if self.load_db:

            str_form.score.data = self.char.str_score or str_form.score.data
            dex_form.score.data = self.char.dex_score or dex_form.score.data
            con_form.score.data = self.char.con_score or con_form.score.data
            int_form.score.data = self.char.int_score or int_form.score.data
            wis_form.score.data = self.char.wis_score or wis_form.score.data
            cha_form.score.data = self.char.cha_score or cha_form.score.data

            str_form.temp.data = self.char.str_temp or str_form.temp.data
            dex_form.temp.data = self.char.dex_temp or dex_form.temp.data
            con_form.temp.data = self.char.con_temp or con_form.temp.data
            int_form.temp.data = self.char.int_temp or int_form.temp.data
            wis_form.temp.data = self.char.wis_temp or wis_form.temp.data
            cha_form.temp.data = self.char.cha_temp or cha_form.temp.data

        ### Dependent ###

        # DEX score and temp set above. Mod is dependent on armor's max dex
        max_dex = self.get_max_dex()

        ### Calculated ###

        str_form.mod.data = get_ability_mod(str_form.score.data + str_form.temp.data)
        dex_form.mod.data = get_ability_mod(dex_form.score.data + dex_form.temp.data)
        try:
            if max_dex < dex_form.mod.data:
                dex_form.mod.data = max_dex
        except TypeError:
            pass
        con_form.mod.data = get_ability_mod(con_form.score.data + con_form.temp.data)
        int_form.mod.data = get_ability_mod(int_form.score.data + int_form.temp.data)
        wis_form.mod.data = get_ability_mod(wis_form.score.data + wis_form.temp.data)
        cha_form.mod.data = get_ability_mod(cha_form.score.data + cha_form.temp.data)

        sign_flag =  ''

        if str_form.mod.data >= 0:
            sign_flag = '+'
        else:
          sign_flag =  ''
        str_form.talespire += sign_flag + str(str_form.mod.data)

        if dex_form.mod.data >= 0:
            sign_flag = '+'
        else:
          sign_flag =  ''
        dex_form.talespire += sign_flag + str(dex_form.mod.data)

        if con_form.mod.data >= 0:
            sign_flag = '+'
        else:
          sign_flag =  ''
        con_form.talespire += sign_flag + str(con_form.mod.data)

        if int_form.mod.data >= 0:
            sign_flag = '+'
        else:
          sign_flag =  ''
        int_form.talespire += sign_flag + str(int_form.mod.data)

        if wis_form.mod.data >= 0:
            sign_flag = '+'
        else:
          sign_flag =  ''
        wis_form.talespire += sign_flag + str(wis_form.mod.data)

        if cha_form.mod.data >= 0:
            sign_flag = '+'
        else:
          sign_flag =  ''
        cha_form.talespire += sign_flag + str(cha_form.mod.data)

        self.abilities_set = True


def prep_abilities(self):
    """ Pre-load ability values such as their names. """

    ability_names = ["STR", "DEX", "CON", "INT", "WIS", "CHA"]
    for i, subform in enumerate(self.form.abilities.entries):
        subform.name_.data = ability_names[i]
        subform.mod.data   = int((subform.temp.data + subform.score.data) / 2) - 5


def get_dex_mod(self):
    """ Returns the DEX modifier. """

    _form = self.form.abilities.entries[1]

    if not self.abilities_set:
        self.get_abilities()

    return _form.mod.data


def get_ability_mod(score: int) -> int:
    return int((score) / 2) - 5


def get_str_score_and_mod(self):
    if not self.abilities_set:
        get_abilities()

    return self.form.abilities.entries[0].score.data, self.form.abilities.entries[0].mod.data


def upload_abilities(self):
    """ Uploads data to the database. """

    str_form = self.form.abilities.entries[0]
    dex_form = self.form.abilities.entries[1]
    con_form = self.form.abilities.entries[2]
    int_form = self.form.abilities.entries[3]
    wis_form = self.form.abilities.entries[4]
    cha_form = self.form.abilities.entries[5]

    checksum = self.checksum([str_form, dex_form, con_form, int_form, wis_form, cha_form])

    if self.char.md5_abilities != checksum:

        self.char.str_score = str_form.score.data
        self.char.str_temp  = str_form.temp.data
        self.char.dex_score = dex_form.score.data
        self.char.dex_temp  = dex_form.temp.data
        self.char.con_score = con_form.score.data
        self.char.con_temp  = con_form.temp.data
        self.char.int_score = int_form.score.data
        self.char.int_temp  = int_form.temp.data
        self.char.wis_score = wis_form.score.data
        self.char.wis_temp  = wis_form.temp.data
        self.char.cha_score = cha_form.score.data
        self.char.cha_temp  = cha_form.temp.data

        self.char.md5_abilities = checksum

        try:
            db.session.commit()
        except: # pragma: no cover
            db.session.rollback()
