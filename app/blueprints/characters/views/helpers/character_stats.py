from numpy import power

from app import db


def get_char_stats(self):
    """ Get character stats from db. """

    _form = self.form.char_stats

    if not self.char_stats_set:

        ### Independent ###

        if self.load_db:
            _form.hp.data             = self.char.hp or _form.hp.data
            _form.hp_max.data         = self.char.hp_max or _form.hp_max.data
            _form.mana.data           = self.char.mana or _form.mana.data
            _form.mana_max.data       = self.char.mana_max or _form.mana_max.data
            _form.base_atk.data       = self.char.base_atk or _form.base_atk.data
            _form.actions.data        = self.char.actions or _form.actions.data
            _form.exhaustion.data     = self.char.exhaustion or _form.exhaustion.data
            _form.speed.data          = self.char.speed or _form.speed.data
            _form.dmg_reduct.data     = self.char.dmg_reduction or _form.dmg_reduct.data
            _form.spell_resist.data   = self.char.spell_resist or _form.spell_resist.data
            _form.arcane_failure.data = self.char.arcane_failure or _form.arcane_failure.data

        # _form.speed_curr.data     = _form.speed.data
        self.data["Speed"] = _form.speed.data

        ### Dependent ###
        max_speed = self.get_max_speed()
        str_score, str_mod = self.get_str_score_and_mod()
        char_size = self.get_size_actual()

        ### Calculated ###

        data = self.data

        # Speed
        try:
            if data["Speed"] > max_speed:
                data["Speed"] = max_speed
        except TypeError:
            data["Speed"] = _form.speed.data

        # Carry Capacity

        size_mods = dict(
            Colossal=16,
            Gargantuan=8,
            Huge=4,
            Large=2,
            Medium=1,
            Small=3/4,
            Tiny=1/2,
            Diminutive=1/4,
            Fine=1/8
        )

        if str_score <= 10:
            data["Heavy Load"] = int(str_score * 10 * size_mods[char_size])
            data["Medium Load"] = int(str_score * 10 * 2/3 * size_mods[char_size])
            data["Light Load"] = int(str_score * 10 * 1/3 * size_mods[char_size])
        else:
            data["Heavy Load"] = int(100 * power(2, (str_score - 10) * 1/5) * size_mods[char_size])
            data["Medium Load"] = int(data["Heavy Load"] * 2/3)
            data["Light Load"] = int(data["Heavy Load"] * 1/3)
        data["Lift Overhead"] = int(data["Heavy Load"])
        data["Lift Load"] = int(data["Heavy Load"] * 2)
        data["Drag Load"] = int(data["Heavy Load"] * 2)


        self.char_stats_set = True


def upload_char_stats(self):
    """ Uploads data to the database. """

    _form = self.form.char_stats
    _char = self.char

    checksum = self.checksum(_form)

    if _char.md5_stats != checksum:

        _char.hp             = _form.hp.data
        _char.hp_max         = _form.hp_max.data
        _char.mana           = _form.mana.data
        _char.mana_max       = _form.mana_max.data
        _char.base_atk       = _form.base_atk.data
        _char.speed          = _form.speed.data
        _char.dmg_reduction  = _form.dmg_reduct.data
        _char.spell_resist   = _form.spell_resist.data
        _char.arcane_failure = _form.arcane_failure.data
        _char.actions        = _form.actions.data
        _char.exhaustion     = _form.exhaustion.data

        _char.md5_stats = checksum

        try:
            db.session.commit()
        except: # pragma: no cover
            db.session.rollback()
