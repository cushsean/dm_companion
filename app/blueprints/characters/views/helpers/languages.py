from app import db
from app.blueprints.characters.forms import LanguageForm
from app.blueprints.characters.models import Languages


def get_languages(self):
    """ Get languages from the db. """

    _form = self.form.languages

    if not self.languages_set:

        languages = []
        if self.load_db:
            languages = Languages.query.filter_by(character_id=self.char.id).all()

        ### Independent ###

        for lang in languages:

            data = dict(
                lang = lang.name,
                model_id = lang.id
            )
            data = {key: value for key, value in data.items() if value is not None}

            _form.append_entry(data)

        ### Dependent ###

        ### Calculated ###

        self.languages_set = True  # Not used, left for consistancy


def add_language(self):
    """ Adds a line to the languages section. """

    self.form.languages.append_entry()


def rm_language(self, index):
    """ Removes the selected record. """

    self.rm_record(self.form.languages, Languages, index)


def upload_languages(self):
    """ Uploads data to the database. """

    _form = self.form.languages
    _char = self.char

    langs = Languages.query.filter_by(character_id=_char.id).all()

    for i, _lang in enumerate(_form.entries):

        if not _lang.lang.data:
            continue

        checksum = self.checksum(_lang)

        if i >= len(langs):
            langs.append(Languages(character_id=_char.id))
            db.session.add(langs[i])
            db.session.flush()
        elif langs[i].md5 == checksum:
            continue

        # Write Changes
        langs[i].name       = _lang.lang.data
        _lang.model_id.data = langs[i].id

        langs[i].md5 = checksum

        try:
            db.session.commit()
        except: # pragma: no cover
            db.session.rollback()
