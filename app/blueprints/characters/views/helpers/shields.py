from app import db
from app.blueprints.characters.forms import ShieldForm
from app.blueprints.characters.models import Shield


def get_shields(self):
    """ Sets the character's shields based on the database. """

    _form = self.form.shields

    if not self.shields_set:

        shields = []
        if self.load_db:
            shields = Shield.query.filter_by(character_id=self.char.id).all()

        ### Independent ###

        for shield in shields:

            data = dict(
                name_    = shield.name,
                ac       = shield.ac,
                check    = shield.check_penalty,
                spell    = shield.spell_failure,
                weight   = shield.weight,
                notes    = shield.notes,
                model_id = shield.id
            )
            data = {key: value for key, value in data.items() if value is not None}

            _form.append_entry(data)

        ### Dependent ###

        ### Calculated ###

        self.shields_set = True  # Not used, left for consistancy


def get_shield_ac(self):
    """ Returns the culative ac bonuses from all shields wield by the character. """

    _form = self.form.shields

    if not self.shields_set:
        self.get_shields()

    shield_ac = 0
    for entrie in _form.entries:
        try:
            shield_ac += entrie.ac.data
        except TypeError as err:
            if entrie.ac.data is None:
                pass
            else:
                print("ERROR: app/blueprints/characters/views/helpers/shields.py --", err)

    return shield_ac


def add_shield(self):
    """ Adds a line to the sheild section. """

    self.form.shields.append_entry()


def rm_shield(self, index):
    """ Removes the selected record. """

    self.rm_record(self.form.shields, Shield, index)


def upload_shields(self):
    """ Uploads data to the database. """

    _form = self.form.shields
    _char = self.char

    db_shields = Shield.query.filter_by(character_id=_char.id).all()

    for i, _shield in enumerate(_form.entries):

        if not _shield.name_.data:
            continue

        checksum = self.checksum(_shield)

        if i >= len(db_shields):
            db_shields.append(Shield(character_id=_char.id))
            db.session.add(db_shields[i])
            db.session.flush()
        elif db_shields[i].md5 == checksum:
            continue

        # Write Changes
        db_shields[i].name          = _shield.name_.data
        db_shields[i].ac            = _shield.ac.data
        db_shields[i].weight        = _shield.weight.data
        db_shields[i].check_penalty = _shield.check.data
        db_shields[i].spell_failure = _shield.spell.data
        db_shields[i].notes         = _shield.notes.data
        _shield.model_id.data       = db_shields[i].id

        db_shields[i].md5 = checksum

        try:
            db.session.commit()
        except: # pragma: no cover
            db.session.rollback()
