# noqa: E501

# from string import strip

from flask_wtf import FlaskForm
from sqlalchemy.exc import OperationalError, ProgrammingError
from wtforms import Form, BooleanField, FloatField, IntegerField, StringField, SubmitField, SelectField, TextField, FormField, FieldList, HiddenField
from wtforms.validators import InputRequired, NumberRange
from wtforms.widgets import TextArea

from app import db


"""
Field Discription Dictionary Protocal
    editable: Boolean - True = Show editable field, False = Show only data
"""


class AbilityForm(Form):

    is_set = False

    name_ = StringField(label="Name",                 description=dict(editable=False, header=True, record=True, sum=False))
    temp  = IntegerField(label="Temp",      default=0, description=dict(editable=True, header=True, record=True, sum=True))
    score = IntegerField(label="Score",    default=0, description=dict(editable=True, header=True, record=True, sum=True), validators=[NumberRange(min=0), InputRequired()])
    mod   = IntegerField(label="Modifier",   default=0, description=dict(editable=False, header=True, record=True, sum=False))
    roll  = StringField(label="Roll", default="Roll", description=dict(editable=False, header=False, record=True, sum=False, talespire=True))
    talespire = "talespire://dice/+1d20"

    class Meta:
        csrf = False


class ACForm(Form):

    is_set = False

    total       = IntegerField(label="Total",             default=10, description=dict(editable=False, header=True, record=True, sum=False),  validators=[NumberRange(min=0)])
    base        = IntegerField(label="Bace AC",           default=10, description=dict(editable=False, header=True, record=True, sum=False),  validators=[NumberRange(min=0)])
    armor       = IntegerField(label="Armor Bonus",       default=0,  description=dict(editable=False, header=True, record=True, sum=False),  validators=[NumberRange(min=0)])
    shield      = IntegerField(label="Shield Bonus",      default=0,  description=dict(editable=False, header=True, record=True, sum=False),  validators=[NumberRange(min=0)])
    dex         = IntegerField(label="DEX Mod",           default=0,  description=dict(editable=False, header=True, record=True, sum=False))
    size        = IntegerField(label="Size Mod",          default=0,  description=dict(editable=False, header=True, record=True, sum=False))
    natural     = IntegerField(label="Natural Armor",     default=0,  description=dict(editable=True, header=True, record=True, sum=True),  validators=[NumberRange(min=0)])
    deflection  = IntegerField(label="Deflection Bonus",  default=0,  description=dict(editable=True, header=True, record=True, sum=True),  validators=[NumberRange(min=0)])
    misc        = IntegerField(label="Misc Bonus",        default=0,  description=dict(editable=True, header=True, record=True, sum=True))

    class Meta:
        csrf = False


class ArmorForm(Form):

    is_set = False

    name_    = StringField(label="Name",   default="", description=dict(editable=True, header=True, record=True, sum=True))
    typ      = StringField(label="Type",   default="", description=dict(editable=True, header=True, record=True, sum=True))
    ac       = IntegerField(label="AC",    default=0,  description=dict(editable=True, header=True, record=True, sum=True))
    dex      = IntegerField(label="DEX",   default=0,  description=dict(editable=True, header=True, record=True, sum=True))
    check    = IntegerField(label="Check", default=0,  description=dict(editable=True, header=True, record=True, sum=True))
    spell    = IntegerField(label="Spell", default=0,  description=dict(editable=True, header=True, record=True, sum=True))
    speed    = IntegerField(label="Speed", default=30, description=dict(editable=True, header=True, record=True, sum=True))
    weight   = FloatField(label="Weight",  default=0,  description=dict(editable=True, header=True, record=True, sum=True))
    notes    = StringField(label="Notes",  default="", description=dict(editable=True, header=True, record=True, sum=True), widget=TextArea())
    bn_del   = SubmitField(label='X',                  description=dict(editable=True, header=False, record=True, sum=False))
    model_id = HiddenField(default=None, description=dict(editable=True, header=False, record=True, sum=False))

    class Meta:
        csrf = False


class CharInfoForm(Form):

    is_set = False
    level_up = False

    char_name       = StringField(label="Name",             default="New Character", description=dict(editable=True, header=True, record=True, sum=True))
    try:
        player      = SelectField(label="Player",           default="",              description=dict(editable=True, header=True, record=True, sum=True), choices=[username[0] for username in db.session.execute('SELECT username from user where 1').all()])
    except (ProgrammingError, OperationalError):
        player      = SelectField(label="Player",           default="",              description=dict(editable=True, header=True, record=True, sum=True), choices=["User's not loaded"])
    ecl             = IntegerField(label="Character Class", default=0,               description=dict(editable=False, header=True, record=True, sum=False), validators=[NumberRange(min=0)])
    race            = StringField(label="Race",             default="",              description=dict(editable=True, header=True, record=True, sum=True))
    lvl_adj         = IntegerField(label="Level Adj",       default=0,               description=dict(editable=True, header=True, record=True, sum=True), validators=[NumberRange(min=0)])
    alignment       = SelectField(label="Alignment",        default="N",             description=dict(editable=True, header=True, record=True, sum=True), choices=['LG', 'LN', 'LE', 'NG', 'N', 'NE', 'CG', 'CN', 'CE'])
    deity           = StringField(label="Deity",            default="",              description=dict(editable=True, header=True, record=True, sum=True))
    size            = SelectField(label="Size",             default="Medium",        description=dict(editable=True, header=True, record=True, sum=True), choices=['Fine', 'Diminutive', 'Tiny', 'Small', 'Medium', 'Large', 'Huge', 'Gargantuan', 'Colossal'])
    age             = IntegerField(label="Age",             default=0,               description=dict(editable=True, header=True, record=True, sum=True))
    gender          = StringField(label="Gender",           default="",              description=dict(editable=True, header=True, record=True, sum=True))
    height          = StringField(label="Height",           default="",              description=dict(editable=True, header=True, record=True, sum=True))
    weight          = StringField(label="Weight",           default="",              description=dict(editable=True, header=True, record=True, sum=True))
    exp             = IntegerField(label="Experience",      default=0,               description=dict(editable=True, header=True, record=True, sum=True), validators=[NumberRange(min=0)])
    next_lvl        = StringField(label="Next Level",       default="0/1000",        description=dict(editable=False, header=True, record=True, sum=False))
    back_story      = TextField(label="Back Story",         default="",              description=dict(editable=True, header=True, record=True, sum=True), widget=TextArea())

    class Meta:
        csrf = False


class CharStatForm(Form):

    is_set = False

    hp              = IntegerField(label="HP",                  default=0, description=dict(editable=True, header=True, record=True, sum=True))
    hp_max          = IntegerField(label="Max HP",              default=0, description=dict(editable=True, header=True, record=True, sum=True))
    mana            = IntegerField(label='Mana', default=0, description=dict(editable=True, header=True, record=True, sum=True))
    mana_max        = IntegerField(label='Max Mana', default=0, description=dict(editable=True, header=True, record=True, sum=True))
    base_atk        = IntegerField(label="Base Attack Bonus",   default=0, description=dict(editable=True, header=True, record=True, sum=True))
    actions         = IntegerField(label="Actions",             default=2, description=dict(editable=True, header=True, record=True, sum=True), validators=[NumberRange(min=1)])
    exhaustion      = IntegerField(label="Exhaustion",          default=0, description=dict(editable=True, header=True, record=True, sum=True), validators=[NumberRange(min=0)])
    # speed_curr      = StringField(label="", default="0/", description=dict(editable=False, header=True, record=True, sum=False))
    speed           = IntegerField(label="Base Speed (ft)",          default=0, description=dict(editable=True, header=True, record=True, sum=True), validators=[NumberRange(min=0)])
    dmg_reduct      = IntegerField(label="Damage Reduction",    default=0, description=dict(editable=True, header=True, record=True, sum=True), validators=[NumberRange(min=0)])
    spell_resist    = IntegerField(label="Spell Resistance",    default=0, description=dict(editable=True, header=True, record=True, sum=True), validators=[NumberRange(min=0)])
    arcane_failure  = IntegerField(label="Arcane Failure (%)",  default=0, description=dict(editable=True, header=True, record=True, sum=True), validators=[NumberRange(min=0)])

    class Meta:
        csrf = False


class ClassForm(Form):

    is_set = False

    name_    = StringField(label="Class",  default="", description=dict(editable=True, header=True, record=True, sum=True))
    level    = IntegerField(label="Level", default=1,  description=dict(editable=True, header=True, record=True, sum=True))
    bn_del   = SubmitField(label='X', description=dict(editable=True, header=False, record=True, sum=False))
    model_id = HiddenField(default=None, description=dict(editable=True, header=False, record=True, sum=False))

    class Meta:
        csrf = False


class FeatForm(Form):

    is_set = False

    feat     = StringField(label="Feat", default="",  description=dict(editable=True, header=True, record=True, sum=True))
    bn_del   = SubmitField(label='X',                 description=dict(editable=True, header=False, record=True, sum=False))
    model_id = HiddenField(default=0, description=dict(editable=True, header=False, record=True, sum=False))

    class Meta:
        csrf = False


class InitForm(Form):

    is_set = False

    total = IntegerField(label="Total",    default=0, description=dict(editable=False, header=True, record=True, sum=False))
    dex   = IntegerField(label="DEX Mod",  default=0, description=dict(editable=False, header=True, record=True, sum=False))
    misc  = IntegerField(label="Misc Mod", default=0, description=dict(editable=True, header=True, record=True, sum=True))
    roll  = StringField(label="Roll", default="Roll", description=dict(editable=False, header=False, record=True, sum=False, talespire=True))
    talespire = "talespire://dice/+1d20"

    class Meta:
        csrf = False


class ItemsForm(Form):

    is_set = False

    name_    = StringField(label="Name",       default="", description=dict(editable=True, header=True, record=True, sum=True))
    quantity = IntegerField(label="Quantity",   default=0,  description=dict(editable=True, header=True, record=True, sum=True), validators=[NumberRange(min=0)])
    value    = StringField(label="Value",       default="", description=dict(editable=True, header=True, record=True, sum=True))
    weight   = FloatField(label="Weight",       default=0,  description=dict(editable=True, header=True, record=True, sum=True), validators=[NumberRange(min=0)])
    notes    = StringField(label="Notes",       default="", description=dict(editable=True, header=True, record=True, sum=True), widget=TextArea())
    bn_del   = SubmitField(label='X',                       description=dict(editable=True, header=False, record=True, sum=False))
    model_id = HiddenField(default=None, description=dict(editable=True, header=False, record=True, sum=False))

    class Meta:
        csrf = False


class LanguageForm(Form):

    is_set = False

    lang     = StringField(label="", default="", description=dict(editable=True, header=True, record=True, sum=True))
    bn_del   = SubmitField(label='X',            description=dict(editable=True, header=False, record=True, sum=False))
    model_id = HiddenField(default=None, description=dict(editable=True, header=False, record=True, sum=False))

    class Meta:
        csrf = False


class MoneyForm(Form):

    is_set = False

    cp = IntegerField(label="Copper Piece",   default=0, description=dict(editable=True, header=True, record=True, sum=True), validators=[NumberRange(min=0)])
    sp = IntegerField(label="Silver Piece",   default=0, description=dict(editable=True, header=True, record=True, sum=True), validators=[NumberRange(min=0)])
    gp = IntegerField(label="Gold Piece",     default=0, description=dict(editable=True, header=True, record=True, sum=True), validators=[NumberRange(min=0)])
    pp = IntegerField(label="Platinum Piece", default=0, description=dict(editable=True, header=True, record=True, sum=True), validators=[NumberRange(min=0)])

    class Meta:
        csrf = False


class NotesForm(Form):

    is_set = False

    notes = StringField(label="Notes", default="", description=dict(editable=True, header=True, record=True, sum=True), widget=TextArea())

    class Meta:
        csrf = False


class ShieldForm(Form):

    is_set = False

    name_    = StringField(label="Name",   default="", description=dict(editable=True, header=True, record=True, sum=True))
    ac       = IntegerField(label="AC",    default=0,  description=dict(editable=True, header=True, record=True, sum=True))
    check    = IntegerField(label="Check", default=0,  description=dict(editable=True, header=True, record=True, sum=True))
    spell    = IntegerField(label="Spell", default=0,  description=dict(editable=True, header=True, record=True, sum=True))
    weight   = FloatField(label="Weight",  default=0,  description=dict(editable=True, header=True, record=True, sum=True))
    notes    = StringField(label="Notes",  default="", description=dict(editable=True, header=True, record=True, sum=True), widget=TextArea())
    bn_del   = SubmitField(label='X',                  description=dict(editable=True, header=False, record=True, sum=False))
    model_id = HiddenField(default=None, description=dict(editable=True, header=False, record=True, sum=False))

    class Meta:
        csrf = False


class SkillForm(Form):

    is_set = False

    class_skill = BooleanField(label="Class Skill?", default=False, description=dict(editable=True, header=True, record=True, sum=True))
    name_       = StringField(label="Name",                         description=dict(editable=False, header=True, record=True, sum=False))
    key         = StringField(label="Key Ability",                  description=dict(editable=False, header=True, record=True, sum=False))
    total       = IntegerField(label="Skill Mod",    default=0,     description=dict(editable=False, header=True, record=True, sum=False, alt_total=True))
    alt_total   = StringField(label="Skill Mod",     default="0",   description=dict(editable=False, header=False, record=False, sum=False))
    ability     = StringField(label="Ability Mod",   default="",    description=dict(editable=False, header=True, record=True, sum=False))
    ranks       = IntegerField(label="Ranks",        default=0,     description=dict(editable=True, header=True, record=True, sum=True), validators=[NumberRange(min=0)])
    class_mod   = IntegerField(label="Class Bonus",  default=0,     description=dict(editable=True, header=True, record=True, sum=True), validators=[NumberRange(min=-5)])
    misc_mod    = IntegerField(label="Misc Mod",     default=0,     description=dict(editable=True, header=True, record=True, sum=True), validators=[NumberRange(min=0)])
    roll        = StringField(label="Roll", default="Roll", description=dict(editable=False, header=False, record=True, sum=False, talespire=True))
    alt_roll    = StringField(label="Roll", default="Roll", description=dict(editable=False, header=False, record=True, sum=False, talespire_alt=True))
    talespire = "talespire://dice/+1d20"
    talespire_alt = "talespire://dice/+1d20"
    talespire_alt_bool = False

    class Meta:
        csrf = False


class SpecialsForm(Form):

    is_set = False

    special_ability = StringField(label="Special Abilities", default="", description=dict(editable=True, header=True, record=True, sum=True))
    bn_del          = SubmitField(label='X',                             description=dict(editable=True, header=False, record=True, sum=False))
    model_id        = HiddenField(default=None, description=dict(editable=True, header=False, record=True, sum=False))

    class Meta:
        csrf = False


class SpellForm(Form):

    is_set = False

    name_    = StringField(label="Spell Name", default="", description=dict(editable=True, header=True, record=True, sum=True))
    mana     = IntegerField(label="Mana Cost", default=0,  description=dict(editable=True, header=True, record=True, sum=True))
    brief    = StringField(label="Brief",      default="", description=dict(editable=True, header=True, record=True, sum=True))
    bn_del   = SubmitField(label='X',                      description=dict(editable=True, header=False, record=True, sum=False))
    model_id = HiddenField(default=None, description=dict(editable=True, header=False, record=True, sum=False))


    class Meta:
        csrf = False


class SpellStatForm(Form):

    is_set = False

    num_known = IntegerField(validators=[NumberRange(min=0)])

    class Meta:
        csrf = False


class WeaponForm(Form):

    is_set = False

    name_     = StringField(label='Weapon',       default="", description=dict(editable=True, header=True, record=True, sum=True))
    atk_bonus = IntegerField(label='Attack Bonus', default=0,  description=dict(editable=True, header=True, record=True, sum=True))
    dmg       = StringField(label='Damage',        default="", description=dict(editable=True, header=True, record=True, sum=True))
    crit      = StringField(label='Critial',       default="", description=dict(editable=True, header=True, record=True, sum=True))
    rng       = IntegerField(label='Range',        default=0,  description=dict(editable=True, header=True, record=True, sum=True))
    dmg_type  = StringField(label='Damage Type',   default="", description=dict(editable=True, header=True, record=True, sum=True))
    notes     = StringField(label='Weapon Notes',  default="", description=dict(editable=True, header=True, record=True, sum=True), widget=TextArea())
    bn_del    = SubmitField(label='X',                         description=dict(editable=True, header=False, record=True, sum=False))
    model_id  = HiddenField(default=None, description=dict(editable=True, header=False, record=True, sum=False))

    class Meta:
        csrf = False


class MainForm(FlaskForm):
    render_kw    = {'class': 'CharSheet'}
    char_info    = FormField(CharInfoForm)
    class_info   = FieldList(FormField(ClassForm),    min_entries=1)
    char_stats   = FormField(CharStatForm)
    ac           = FormField(ACForm)
    init         = FormField(InitForm)
    abilities    = FieldList(FormField(AbilityForm),  min_entries=6)
    skills       = FieldList(FormField(SkillForm),    min_entries=20)
    weapons      = FieldList(FormField(WeaponForm),   min_entries=0)
    armor        = FieldList(FormField(ArmorForm),    min_entries=0)
    shields      = FieldList(FormField(ShieldForm),   min_entries=0)
    spells       = FieldList(FormField(SpellForm),    min_entries=0)
    items        = FieldList(FormField(ItemsForm),    min_entries=0)
    money        = FormField(MoneyForm)
    feats        = FieldList(FormField(FeatForm),     min_entries=0)
    specials     = FieldList(FormField(SpecialsForm), min_entries=0)
    languages    = FieldList(FormField(LanguageForm), min_entries=0)
    notes        = FormField(NotesForm)
    add_class    = SubmitField(label="Multiclass", render_kw=dict(formaction=''))
    add_weapon   = SubmitField(label="Add Weapon", render_kw=dict(formaction='#weapons_header'))
    add_armor    = SubmitField(label="Add Armor", render_kw=dict(formaction='#armor_header'))
    add_shield   = SubmitField(label="Add Shield", render_kw=dict(formaction='#shields_header'))
    add_spell    = SubmitField(label="Add Spell", render_kw=dict(formaction='#spells_header'))
    add_item     = SubmitField(label="Add Item", render_kw=dict(formaction='#items_header'))
    add_feat     = SubmitField(label="Add Feat", render_kw=dict(formaction='#feats_header'))
    add_special  = SubmitField(label="Add Special", render_kw=dict(formaction='#specials_header'))
    add_language = SubmitField(label="Add Language", render_kw=dict(formaction='#languages_header'))
    save         = SubmitField(label='Save', render_kw=dict(formaction=''))
    update       = SubmitField(label='Update', render_kw=dict(formaction=''))


class SubListForm(FlaskForm):
    char_id = HiddenField()
    char_name = SubmitField()
    rm_btn = SubmitField(label='X')

    class Meta:
        csrf = False


class ListForm(FlaskForm):
    new_character = SubmitField(label='Create New Character')
    char = FieldList(FormField(SubListForm))
