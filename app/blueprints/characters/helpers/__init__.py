from app.blueprints.characters.models import Character


def get_equal():
    return dict(header="", value="=")


def get_plus():
    return dict(header="", value="+")


def get_character(character_id: int = None):

    if character_id is None:
        return None

    character = Character.query.filter_by(id=character_id).first()

    return character
