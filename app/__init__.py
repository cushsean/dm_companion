import click

from flask import Flask, config, redirect, url_for
from flask.cli import with_appcontext
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_table import Table, Col


# Create Global instances
db = SQLAlchemy()
login_manager = LoginManager()
bcrypt = Bcrypt()
migrate = Migrate()


def create_app(test_config=None):
    # CONFIGURATION
    if test_config is None:
        app = Flask(__name__, instance_relative_config=True) # pragma: no cover

        # Load base config
        app.config.from_object('config') # pragma: no cover

        # Load instance config
        app.config.from_pyfile('config.py', silent=True) # pragma: no cover

    else:
        app = Flask(__name__, instance_relative_config=False)

        # Load test suit config
        app.config.from_pyfile(test_config)

    # LINK GLOBAL INSTANCES
    db.init_app(app)
    migrate.init_app(app, db)
    bcrypt.init_app(app)
    login_manager.init_app(app)

    # CUSTOM COMMANDS AND FUNCTIONS
    @app.cli.command('db-init')
    def init_db_command(): # pragma: no cover
        """Clear the existing data and create new tables."""
        db.create_all()
        db_init()
        try:
            db.session.commit()
            click.echo('Initialized the database.')
        except:
            db.session.rollback()
            click.echo('Something went wronge on initialization.')

    with app.app_context():
        # REGISTER VIEWS
        from app.blueprints import admin, auth, characters, economy, home, library, mechanics, user

        app.register_blueprint(admin.bp)
        app.register_blueprint(auth.bp)
        app.register_blueprint(characters.bp)
        app.register_blueprint(economy.bp)
        app.register_blueprint(home.bp)
        app.register_blueprint(library.bp)
        app.register_blueprint(mechanics.bp)
        app.register_blueprint(user.bp)

        # OPEN HOME PAGE
        @app.route('/')
        def open_home():
            return home.index()

        return app


def db_init():
    """ Initializes the database with preliminary information. """
    if not db.session.execute("SELECT * FROM privilege_level"):
        db.session.execute("INSERT INTO privilege_level (name, level) VALUES ('player', 10), ('admin', 1), ('dm', 5);")
    # The first user to be created will be an admin.
